/*
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/*
 *
 * based on Polyglot code
 *
 */
#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "common.h"
#include "board.h"
#include "engine.h"
#include "move.h"

struct entry_t {
   uint64_t key;
   uint16_t move;
   uint16_t count;
   uint16_t n;
   uint16_t sum;
};

static FILE * book_file;
static int book_size;

static int find_pos(uint64_t key);
static void read_entry(struct entry_t *entry, int n);
static uint64_t read_integer(FILE *file, int size);

int 
my_random(int n) 
{
	double r;

	r = (double)(rand()) / ((double)(RAND_MAX) + 1.0);

	return (int)(floor(r * (double)(n)));
}

void 
book_init(void) 
{
   book_file = NULL;
   book_size = 0;
}

void 
book_open(const char file_name[]) 
{
   book_file = fopen(file_name, "rb");

   if (book_file != NULL) {
	  if (fseek(book_file, 0, SEEK_END) == -1)
		 fprintf(stderr, "book_open(): fseek(): %s\n", strerror(errno));

	  book_size = ftell(book_file) / 16;
	  if (book_size == -1) 
		  fprintf(stderr, "book_open(): ftell(): %s\n", strerror(errno));
   } else
	   fprintf(stderr, "Unable open book\n");
}

void 
book_close(void) 
{
   if (book_file != NULL && fclose(book_file) == EOF)
	  fprintf(stderr, "book_close(): fclose(): %s\n", strerror(errno));
}

int 
book_move(void) 
{
	int i, pos, from, to;
	int move, score;
	int best_move, best_score;
	struct entry_t entry[1];
	struct moves_t moves;

	if (book_file != NULL && book_size != 0) {
		/* draw a move according to a fixed probability distribution */
		best_move = 0;
		best_score = 0;

		for (pos = find_pos(brd.hash_key); pos < book_size; pos++) {
			read_entry(entry, pos);

			if (entry->key != brd.hash_key) 
				break;

			move = entry->move;
			score = entry->count;

			best_score += score;
			if (my_random(best_score) < score) 
				best_move = move;
		}

		if (best_move != 0) {
			/* convert polyglot move to our format */
			from = SET_FROM(((best_move >> 9) & 7) * 8 + \
					((best_move >> 6) & 7));
			to = SET_TO(((best_move >> 3) & 7) * 8 + (best_move & 7));

			if (square64[from] == KING) {
				if (GET_FROM(from) == E1 && GET_TO(to) == H1)
					to = SET_TO(G1);
				else if (GET_FROM(from) == E1 && GET_TO(to) == A1)
					to = SET_TO(C1);
				else if (GET_FROM(from) == E8 && GET_TO(to) == H8)
					to = SET_TO(G8);
				else if (GET_FROM(from) == E8 && GET_TO(to) == A8)
					to = SET_TO(C8);
			}

			gen_moves(&moves);

			if (!moves.count)
				return 0;

			for (i = 0; i < moves.count; i++) {
				if (!make_move(moves.move[i], TRUE))
					continue;		
				if ((moves.move[i] & 0xfff) == (from + to))
					return moves.move[i];
			}
		}
	}

	return 0;
}

static int 
find_pos(uint64_t key) 
{
	int left, right, mid;
	struct entry_t entry[1];

	/* binary search (finds the leftmost entry) */
	left = 0;
	right = book_size - 1;

	while (left < right) {
		mid = (left + right) / 2;
		read_entry(entry, mid);

		if (key <= entry->key)
			right = mid;
		else
			left = mid + 1;
	}
	read_entry(entry, left);

	return (entry->key == key) ? left : book_size;
}

static void 
read_entry(struct entry_t *entry, int n) 
{
	if (fseek(book_file,n*16,SEEK_SET) == -1)
		fprintf(stderr, "read_entry(): fseek(): %s\n",strerror(errno));

	entry->key = read_integer(book_file, 8);
	entry->move = read_integer(book_file, 2);
	entry->count = read_integer(book_file, 2);
	entry->n = read_integer(book_file, 2);
	entry->sum = read_integer(book_file, 2);
}

static uint64_t 
read_integer(FILE *file, int size) 
{
	int i, b;
	uint64_t n;

	n = 0;
	for (i = 0; i < size; i++) {
		b = fgetc(file);

		if (b == EOF) {
			if (feof(file))
				fprintf(stderr, "read_integer(): fgetc(): EOF reached\n");
			else
				fprintf(stderr, "read_integer(): fgetc(): %s\n", \
						strerror(errno));
		}
		n = (n << 8) | b;
	}

	return n;
}
