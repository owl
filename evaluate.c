/*
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#include "common.h"
#include "attacks.h"
#include "board.h"
#include "engine.h"
#include "evaluate.h"
#include "move.h"
#include "trans.h"

#define OPENING_SCORE	(side_score[OPENING][brd.wtm] - \
		side_score[OPENING][1 ^ brd.wtm])
#define ENDGAME_SCORE	(side_score[ENDGAME][brd.wtm] - \
		side_score[ENDGAME][1 ^ brd.wtm])

static int rank_flip[2][8] = { 
	{ 0, 1, 2, 3, 4, 5, 6, 7 },
	{ 7, 6, 5, 4, 3, 2, 1, 0 }
};

/* cental pawns on their initial squares */
const uint64_t d2e2[2] = { 
	0x0000000000001800ULL, 0x0018000000000000ULL 
};
/* bitmask for 7th rank */
const uint64_t rank7bb[2] = { 
	0x00FF000000000000ULL, 0x000000000000FF00ULL 
};

static int king_attacks[2];
static int king_attackers[2];
static int king_score[2][2];
static int mobility_score[2][2];
static int pawn_score[2][2];
static int position_score[2][2];
static int side_score[2][2];

/* passed pawns bitboard */
static uint64_t passers[2];

/* bonus for side to move */
static int wtm_bonus[2] = {25, 10};

/* forward declarations of functions */
static void append_scores(void);
static void evaluate_pawns(int side);
static void evaluate_knights(int side);
static void evaluate_bishops(int side);
static void evaluate_rooks(int side);
static void evaluate_queens(int side);
static void evaluate_king(int side);
static int evaluate_draw_pattern(void);
static int evaluate_mate(int side);
static void evaluate_material_imbalances(int side);
static void evaluate_blocked_pieces(int side);
static void zero_scores(void);

static inline void 
append_scores(void)
{
	side_score[OPENING][WHITE] += king_score[OPENING][WHITE] + \
		mobility_score[OPENING][WHITE] + pawn_score[OPENING][WHITE] + \
		position_score[OPENING][WHITE];
	side_score[OPENING][BLACK] += king_score[OPENING][BLACK] + 
		mobility_score[OPENING][BLACK] + pawn_score[OPENING][BLACK] + \
		position_score[OPENING][BLACK];
	side_score[ENDGAME][WHITE] += king_score[ENDGAME][WHITE] + \
		mobility_score[ENDGAME][WHITE] + pawn_score[ENDGAME][WHITE] + \
		position_score[ENDGAME][WHITE];
	side_score[ENDGAME][BLACK] += king_score[ENDGAME][BLACK] + \
		mobility_score[ENDGAME][BLACK] + pawn_score[ENDGAME][BLACK] + \
		position_score[ENDGAME][BLACK];
}

/* bonus for rook behind passed pawn */
static int rook_behind_pp = 30;

/* bonus for having more pawns in endgame if there are no pieces
   on the board */
static int more_pawns_bonus = 15;
static int lazy_margin = 250;

static uint64_t pawn_attacks[2];

/* return TRUE if current position is DRAW */
int 
evaluate_draw(void)
{
	if (brd.fifty_rule >= 100) 
		return TRUE;	/* draw by fifty rule */

	if (material_pawns[WHITE] || material_pawns[BLACK]) 
		return FALSE;	/* there are some pawns on the board */

	/* check for draw by insufficient material */
	if (((material_complete[WHITE] < piece_value[ROOK]) || \
			piece_count[WHITE][KNIGHT] == 2) && \
			((material_complete[BLACK] < piece_value[ROOK]) || \
			 piece_count[BLACK][KNIGHT] == 2))
		return TRUE;

	return FALSE;
}

/* evaluate current position */
int 
evaluate(int alpha, int beta)
{
	int score;
	int mate_score;
	int side;
	int xside;

	assert(alpha >= -MATE_VALUE && beta <= MATE_VALUE);
	assert(beta > alpha);

	/* we should find mate position */
	if ((!material_pawns[WHITE] && !material_complete[BLACK]) || \
			(!material_pawns[BLACK] && !material_complete[WHITE])) {
		mate_score = evaluate_mate(WHITE) - evaluate_mate(BLACK);
		if (mate_score)
			return brd.wtm ? -mate_score : mate_score;
	}

	/* check for drawish endgame */
	if (evaluate_draw_pattern())
		return 0;

#ifdef STATISTIC_COUNTERS
	counters.evaluations++;
#endif
	zero_scores();

	side_score[OPENING][WHITE] = material_complete[WHITE] - wpc * 10;
	side_score[OPENING][BLACK] = material_complete[BLACK] - bpc * 10;
	side_score[ENDGAME][WHITE] = material_complete[WHITE];
	side_score[ENDGAME][BLACK] = material_complete[BLACK];

	/* wtm bonus */
	side_score[OPENING][brd.wtm] += wtm_bonus[OPENING];
	side_score[ENDGAME][brd.wtm] += wtm_bonus[ENDGAME];

	pawn_attacks[WHITE] = ((PAWNS(WHITE) & ~file_bit[7]) << 9) | \
						  ((PAWNS(WHITE) & ~file_bit[0]) << 7);
	pawn_attacks[BLACK] = ((PAWNS(BLACK) & ~file_bit[0]) >> 9) | \
						  ((PAWNS(BLACK) & ~file_bit[7]) >> 7);

	score = (OPENING_SCORE * (256 - GAME_PHASE) + \
			ENDGAME_SCORE * GAME_PHASE) / 256;

	int lazy_eval = FALSE;
	if (alpha == beta - 1 && ((score - lazy_margin >= beta) || (score + lazy_margin <= alpha))) {
		lazy_eval = TRUE;
		counters.lazy_evaluations++;
	}

	for (side = WHITE; side <= BLACK; side++) {
		evaluate_pawns(side);
		evaluate_material_imbalances(side);
		evaluate_blocked_pieces(side);
		if (!lazy_eval) {
			evaluate_knights(side);
			evaluate_bishops(side);
			evaluate_rooks(side);
			evaluate_queens(side);
			evaluate_king(side);
		}
	}

	/* look for rook behind passer */
	for (side = WHITE; side <= BLACK; side++) {
		int square;
		uint64_t pp;
		uint64_t bb = ROOKS(side);

		while (bb) {
			square = bit_scan_clear(&bb);
			pp = file_bit[_FILE(square)] & passers[side];
			if (pp) {
				if (side == WHITE) {
					if (_RANK(bit_scan(pp)) > _RANK(square))
						position_score[ENDGAME][side] += rook_behind_pp;
				} else {
					if (_RANK(bit_scan(pp)) < _RANK(square))
						position_score[ENDGAME][side] += rook_behind_pp;
				}
			}
		}
	}

	/* bonus for having more pawns if there are now pieces on the board */
	if (!material_pieces[WHITE] && !material_pieces[BLACK])
		side_score[ENDGAME][WHITE] += (wpc - bpc) * more_pawns_bonus;
	
	append_scores();

	/* bishop of opposite colours */
	if (wqc + wrc + wnc + bqc + brc + bnc == 0 && wbc == 1 && \
			bbc == 1 && abs(wpc - bpc) <= 2) {
		if (popcount((BISHOPS(WHITE) | BISHOPS(BLACK)) & WHITESQUARES) == 1) {
			side_score[ENDGAME][WHITE] /= 2;
			side_score[ENDGAME][BLACK] /= 2;
		}
	}

	/* get final score according to game phase */
	score = (OPENING_SCORE * (256 - GAME_PHASE) + \
			ENDGAME_SCORE * GAME_PHASE) / 256;
	assert(score >= -MATE_VALUE && score <= MATE_VALUE);

	side = brd.wtm;
	xside = 1 ^ side;

	/* can't win if we have two knights or a bishop and no pawns
	   (but theoretically we can, for example so called 'cooperative mate') */
	if (score > 0 && !material_pawns[side] && \
		((material_pieces[side] < piece_value[ROOK]) || \
		 ((KNIGHTS(side) | KING(side)) == side_boards[side]))) {
		score = 0;
	} else if (score < 0 && !material_pawns[xside] && \
		((material_pieces[xside] < piece_value[ROOK]) || \
		 ((KNIGHTS(xside) | KING(xside)) == side_boards[xside]))) {
		score = 0;
	}

	if (wqc == bqc && wrc == brc && abs(wbc + wnc - bbc - bnc) < 2) {
		if (!wpc && !bpc) {
			/* no pawns and one side is ahead by minor piece */
			score /= 2;
		} else {
			if (score > 0) {
				if (side == WHITE && (wbc + wnc - bbc - bnc == 1) && !wpc && bpc) {
					score /= bpc + 1;
				} else if (side == BLACK && (bbc + bnc - wbc - wnc == 1) && !bpc && wpc) {
					score /= wpc + 1;
				}
			}
		}
	}

	/* ROOK vs [KNIGHT|BISHOP] + (PAWN)? */
	if ((material_pieces[WHITE] == piece_value[ROOK] && !wpc) && \
		((material_pieces[BLACK] == piece_value[KNIGHT] && bpc <= 1) || \
		 (material_pieces[BLACK] == piece_value[BISHOP] && bpc <= 1)))
		score /= 8;
	else if ((material_pieces[BLACK] == piece_value[ROOK] && !bpc) && \
		((material_pieces[WHITE] == piece_value[KNIGHT] && wpc <= 1) || \
		 (material_pieces[WHITE] == piece_value[BISHOP] && wpc <= 1)))
		score /= 8;

	return score;
}

static int pawn_square_op[2][64] = {
	{
		-15, -5, 0, 5, 5, 0, -5, -15, 
		-15, -5, 0, 5, 5, 0, -5, -15, 
		-15, -5, 0, 15, 15, 0, -5, -15, 
		-15, -5, 0, 25, 25, 0, -5, -15, 
		-15, -5, 0, 15, 15, 0, -5, -15, 
		-15, -5, 0, 5, 5, 0, -5, -15, 
		-15, -5, 0, 5, 5, 0, -5, -15, 
		-15, -5, 0, 5, 5, 0, -5, -15
	},
	{
		-15, -5, 0, 5, 5, 0, -5, -15, 
		-15, -5, 0, 5, 5, 0, -5, -15, 
		-15, -5, 0, 5, 5, 0, -5, -15, 
		-15, -5, 0, 15, 15, 0, -5, -15, 
		-15, -5, 0, 25, 25, 0, -5, -15, 
		-15, -5, 0, 15, 15, 0, -5, -15, 
		-15, -5, 0, 5, 5, 0, -5, -15, 
		-15, -5, 0, 5, 5, 0, -5, -15
	}
};

static int pawn_square_eg[2][64] = {
	{
		  0,  0,  0,   0,	0,	 0,   0,  0,
		-1,  -0, -0,  -0,  -0,	-0,  -0, -1,
		-1,   0,  0,   0,	0,	 0,   0, -1,
		 0,   0,  0,   5,	5,	 0,   0,  0,
		 0,   5,  5,  10,	10,  5,   5,  0,
		 0,   5,  5,  10,	10,  5,   5,  0,
		 5,  15, 15,  15,	15, 15,  15,  5,
		 0,   0,  0,   0,	0,	 0,   0,  0
	},
	{
		 0,   0,  0,   0,	0,	 0,   0,  0,
		 5,  15, 15,  15,	15, 15,  15,  5,
		 0,   5,  5,  10,	10,  5,   5,  0,
		 0,   5,  5,  10,	10,  5,   5,  0,
		 0,   0,  0,   5,	5,	 0,   0,  0,
		-1,   0,  0,   0,	0,	 0,   0, -1,
		-1,  -0, -0,  -0,  -0,	-0,  -0, -1,
		 0,   0,  0,   0,	0,	 0,   0,  0
	}
};
/* bonus for passed pawns */
static int passer_score_op[8] = {0, 0, 10, 15, 30, 50, 90, 0};
static int passer_score_eg[8] = {0, 0, 20, 35, 90, 140, 180, 0};
/* bonus for candidate passers */
static int can_passed_score_op[8] = {0, 0, 5, 10, 20, 35, 50, 0};
static int can_passed_score_eg[8] = {0, 0, 10, 20, 40, 70, 100, 0};
/* penalty for isolated pawns */
static int isolated_penalty_op = -9;
static int isolated_penalty_eg = -18;
/* penalty for isolated pawns on open files */
static int weak_isolated_penalty_op = -18;
static int weak_isolated_penalty_eg = -18;
/* penalty for doubled pawns */
static int doubled_penalty_op[9] = {0, 0, -35, -28, -23, -16, -12, -12, -12};
static int doubled_penalty_eg[9] = {0, 0, -45, -39, -33, -28, -23, -16, -12};
/* penalty for backward pawns */
static int backward_penalty_op = -8;
static int backward_penalty_eg = -10;
/* bonus for unstoppable */
static int unstoppable_bonus = 777;

static void 
evaluate_pawns(int side)
{
	uint64_t b, bb;
	int i;
	int count;
	int square;
	int score_op, score_eg;
	int xside;

	int passed;
    int next_square;
    int candidate;
	int side_defenders, xside_attackers;
	struct pawn_hash_entry_t *phe;

	score_op = 0;
	score_eg = 0;
	xside = 1 ^ side;
	passers[side] = 0ULL;
	bb = PAWNS(side);

	phe = pawn_hash[side] + (brd.hash_pawn_key & pawn_hash_mask);

	if (e.pawn_hash_enabled && phe->key == brd.hash_pawn_key) {
		score_op = phe->score_op;
		score_eg = phe->score_eg;
		passers[side] = phe->passers;
#ifdef STATISTIC_COUNTERS		
		counters.pawn_hash_evaluations++;
#endif
	} else {
		while (bb) {
			passed = FALSE;
			square = bit_scan_clear(&bb);

			/* search passers */
			if (!(forward_ray[side][square] & PAWNS(side))) {
				/* this is most advanced pawn */
				if (!(PAWNS(xside) & passer_mask[side][square])) {
					/* no opponents pawns on adjacent files */
					score_op += passer_score_op[rank_flip[side][_RANK(square)]];
					score_eg += passer_score_eg[rank_flip[side][_RANK(square)]];

					next_square = side? square - 8 : square + 8;
					if ((BIT(next_square) & complete) || 
							(pawn_moves[side][next_square] & PAWNS(xside))) {
						score_op -= passer_score_op[rank_flip[side][_RANK(square)]] / 2;
						score_eg -= passer_score_eg[rank_flip[side][_RANK(square)]] / 2;
                    }

					SET(passers[side], square);

					passed = TRUE;

				} else if (!(forward_ray[side][square] & PAWNS(xside))) {
					/* no opponent's pawns on the same file */
                    candidate = TRUE;
                    uint64_t candidate_way = forward_ray[side][square] |
                        BIT(square);
                    
                    while (candidate_way) {
                        next_square = bit_scan_clear(&candidate_way);
                        
                        if (rank_flip[side][_RANK(next_square)] == 7)
                            break;

                        if (popcount(pawn_moves[xside][next_square] & PAWNS(xside)) >
                            popcount(pawn_moves[side][next_square] & PAWNS(side))) {
                            candidate = FALSE;
                            break;
                        }
                    }

                    if (candidate) {
                        score_op += can_passed_score_op[rank_flip[side][_RANK(square)]];
                        score_eg += can_passed_score_eg[rank_flip[side][_RANK(square)]];
                    }

				}
			}
			if (isolated_mask[_FILE(square)] & PAWNS(side)) {
				/* pawn is not isolated, but maybe backward? */
				if (!((passer_mask[xside][square + (side == WHITE? 8 : -8)] & \
								isolated_mask[_FILE(square)]) & PAWNS(side))) {
					/* pawn is somewhat backward */
					b = pawn_moves[side][square];
					if (side == WHITE)
						b <<= 8;
					else
						b >>= 8;

					if (b & PAWNS(xside)) {
						/* next square is attacked by opponents pawn */
						score_op += backward_penalty_op;
						score_eg += backward_penalty_eg;

						/* backward on semiopen file */
						if ((forward_ray[side][square] & PAWNS(xside)) == 0)
							score_op += backward_penalty_op;
					}
				}
			} else {
				if (!passed) {
					/* pawn is isolated */
					if (!(forward_ray[side][square] & \
								(PAWNS(side) | PAWNS(xside)))) {
						/* isolated on semiopen file */
						score_op += weak_isolated_penalty_op;
						score_eg += weak_isolated_penalty_eg;
					} else {
						score_op += isolated_penalty_op;
						score_eg += isolated_penalty_eg;
					}
				}
			}

			score_op += pawn_square_op[side][square];
			score_eg += pawn_square_eg[side][square];
		}

		/* evaluate doubled pawns */
		for (i = 0; i < 8; i++) {
			count = popcount(file_bit[i] & PAWNS(side));
			if (count > 1) {
				score_op += doubled_penalty_op[piece_count[side][PAWN]] * (count - 1);
				score_eg += doubled_penalty_eg[piece_count[side][PAWN]] * (count - 1);
			}
		}

		/* store scores in hashtable */
		if (e.pawn_hash_enabled) {
			phe->key = brd.hash_pawn_key;
			phe->score_op = score_op;
			phe->score_eg = score_eg;
			phe->passers = passers[side];
		}
	}

	bb = passers[side];
	while (bb) {
		square = bit_scan_clear(&bb);
		score_eg += 5 - distance[square][brd.kings[side]] * 5 +
			distance[square][brd.kings[xside]] * 5;

        /* check for unstoppable passers */
        if (!material_pieces[xside] && 
                !(forward_ray[side][square] & complete)) {
            int promotion_square;
            if (side == WHITE)
                promotion_square = _FILE(square) + 56;
            else
                promotion_square = _FILE(square);
            int distance_diff = 
                distance[promotion_square][brd.kings[xside]] -
                distance[promotion_square][square];
            if (distance_diff > 1) {
                score_eg += unstoppable_bonus;
            } else if (side && distance_diff == 1 && side == brd.wtm) {
                score_eg += unstoppable_bonus;
            }
        }
	}

	pawn_score[OPENING][side] += score_op;
	pawn_score[ENDGAME][side] += score_eg;
}

static int knight_square_op[2][64] = {
	{
		-35, -25, -20, -15, -15, -20, -25, -35,
		-20, -15, -10, -5,	-5,  -10, -15, -20,
		-10, -5,   0,	5,	 5,   0,  -5,  -10,
		-5,   0,   5,  10,	 10,  5,   0,  -5,
		-5,   5,  10,  15,	 15,  10,  5,  -5,
		-3,   5,  10,  10,	 10,  10,  5,  -3,
		-10, -5,   0,	5,	 5,   0,  -5,  -10,
		-25, -15, -10, -10, -10, -10, -15, -25
	},
	{
		-25, -15, -10, -10, -10, -10, -15, -25,
		-10, -5,   0,	5,	 5,   0,  -5,  -10,
		-3,   5,  10,  10,	 10,  10,  5,  -3,
		-5,   5,  10,  15,	 15,  10,  5,  -5,
		-5,   0,   5,  10,	 10,  5,   0,  -5,
		-10, -5,   0,	5,	 5,   0,  -5,  -10,
		-20, -15, -10, -5,	-5,  -10, -15, -20,
		-35, -25, -20, -15, -15, -20, -25, -35
	}
};

static int knight_square_eg[64] = {
	-30, -20, -15, -10, -10, -15, -20, -30,
	-20, -15, -10, -5,	-5,  -10, -15, -20,
	-15, -10,  0,	5,	 5,   0,  -10, -15,
	-10, -5,   5,	10,  10,  5,  -5,  -10,
	-10, -5,   5,	10,  10,  5,  -5,  -10,
	-15, -10,  0,	5,	 5,   0,  -10, -15,
	-20, -15, -10, -5,	-5,  -10, -15, -20,
	-30, -20, -15, -10, -10, -15, -20, -30
};

static int knight_outpost[2][64] = {
	{
		0, 0, 0,  0,  0, 0, 0, 0,
		0, 0, 0,  0,  0, 0, 0, 0,
		0, 0, 0,  0,  0, 0, 0, 0,
		0, 5, 9, 15, 15, 9, 5, 0,
		0, 5, 9, 18, 18, 9, 5, 0,
		0, 0, 5,  9,  9, 5, 0, 0,
		0, 0, 0,  0,  0, 0, 0, 0,
		0, 0, 0,  0,  0, 0, 0, 0
	},
	{
		0, 0, 0,  0,  0, 0, 0, 0,
		0, 0, 0,  0,  0, 0, 0, 0,
		0, 0, 5,  9,  9, 5, 0, 0,
		0, 5, 9, 18, 18, 9, 5, 0,
		0, 5, 9, 15, 15, 9, 5, 0,
		0, 0, 0,  0,  0, 0, 0, 0,
		0, 0, 0,  0,  0, 0, 0, 0,
		0, 0, 0,  0,  0, 0, 0, 0
	}
};

static int knight_mobility_op[9] = {-15, -10, -5, 0, 4, 8, 12, 16, 20};
static int knight_mobility_eg[9] = {-15, -10, -5, 0, 4, 8, 12, 16, 20};
static int knight_bonus[17] = {-12, -11, -11, -10, -10, -8, -8, -5, -5, 0, 0,
	5, 5, 8, 8, 10, 10};

/* evaluate knights */
static void 
evaluate_knights(int side)
{
	int square;
	int xside;
	int xk;
	int mobility;
	uint64_t my_knights;
	uint64_t mobility_xmask;

	xside = 1 ^ side;
	xk = brd.kings[xside];
	my_knights = KNIGHTS(side);

	if (my_knights)
		mobility_xmask = ~(side_boards[side] | (pawn_attacks[xside] & ~complete));

	while (my_knights) {
		square = bit_scan_clear(&my_knights);

		/* piece square tables */
		position_score[OPENING][side] += knight_square_op[side][square];
		position_score[ENDGAME][side] += knight_square_eg[square];

		if (knight_outpost[side][square])
			/* is there enemy pawn that can potentially attack 
			   our knight? */
			if (!(PAWNS(xside) & (passer_mask[side][square] & \
							isolated_mask[_FILE(square)])))
				if (pawn_moves[xside][square] & PAWNS(side)) {
					/* knight is supported by our pawn */
					position_score[OPENING][side] += knight_outpost[side][square];
				}

		/* calculate mobility */
		mobility = popcount(piece_moves[KNIGHT][square] & mobility_xmask);
		mobility_score[OPENING][side] += knight_mobility_op[mobility];
		mobility_score[ENDGAME][side] += knight_mobility_eg[mobility];

		/* value of the knight depending of the pawns count */
		position_score[OPENING][side] += knight_bonus[wpc + bpc];
		position_score[ENDGAME][side] += knight_bonus[wpc + bpc];

		/* king attacks */
		if ((piece_moves[KING][xk] | BIT(xk)) &
			piece_moves[KNIGHT][square]) {
			king_attacks[side]++;
			king_attackers[side]++;
		}
	}
}

static int bishop_square_op[2][64] = {
	{
		-10, -10, -10, -5,	-5,  -10, -10, -10,
		-5,  -5,  -5,	0,	 0,  -5,  -5,  -5,
		-3,  -1,   2,	3,	 3,   2,  -1,  -3,
		-2,   0,   3,	5,	 5,   3,   0,  -2,
		-2,   0,   3,	5,	 5,   3,   0,  -2,
		-3,  -1,   2,	5,	 5,   2,  -1,  -2,
		-5,  -2,   0,	0,	 0,   0,  -2,  -5,
		-10, -10, -10, -10, -10, -10, -10, -10
	},
	{
		-10, -10, -10, -10, -10, -10, -10, -10,
		-5,  -2,   0,	0,	 0,   0,  -2,  -5,
		-3,  -1,   2,	5,	 5,   2,  -1,  -2,
		-2,   0,   3,	5,	 5,   3,   0,  -2,
		-2,   0,   3,	5,	 5,   3,   0,  -2,
		-3,  -1,   2,	3,	 3,   2,  -1,  -3,
		-5,  -5,  -5,	0,	 0,  -5,  -5,  -5,
		-10, -10, -10, -5,	-5,  -10, -10, -10
	}
};

static int bishop_square_eg[64] = {
	-10, -10, -10, -5,	-5,  -10, -10, -10,
	-5,  -5,  -5,	0,	 0,  -5,  -5,  -5,
	-3,  -1,   2,	3,	 3,   2,  -1,  -3,
	-2,   0,   3,	5,	 5,   3,   0,  -2,
	-2,   0,   3,	5,	 5,   3,   0,  -2,
	-3,  -1,   2,	5,	 5,   2,  -1,  -2,
	-5,  -5,  -5,	0,	 0,  -5,  -5,  -5,
	-10, -10, -10, -5,	-5, -10, -10,  -10
};

static int bishop_mobility[16] = {-15, -10, -5, -1, 2, 6, 10, 14, 18, 21, 24,
    27, 30, 32, 33, 34};

/* evaluate bishops */
static void 
evaluate_bishops(int side)
{
	int square;
	int mobility;
	int xk;
	int xside;
	uint64_t attacks;
	uint64_t mobility_xmask;
	uint64_t my_bishops;

	xside = 1 ^ side;
	xk = brd.kings[xside];
	my_bishops = BISHOPS(side);

	if (my_bishops)
		/* exclude these squares from mobility */
		mobility_xmask = ~(side_boards[side] | (pawn_attacks[xside] & ~complete));

	while (my_bishops) {
		square = bit_scan_clear(&my_bishops);

		attacks = BISHOP_ATTACKS(square, complete);

		/* calculate mobility */
		mobility = popcount(attacks & mobility_xmask);
		mobility_score[OPENING][side] += bishop_mobility[mobility];
		mobility_score[ENDGAME][side] += bishop_mobility[mobility];

		position_score[OPENING][side] += bishop_square_op[side][square];
		position_score[ENDGAME][side] += bishop_square_eg[square];

		/* king attacks */
		if (attacks & (piece_moves[KING][xk] | BIT(xk))) {
			king_attacks[side]++;
			king_attackers[side]++;
		}
	}
}

static int rook_on_semiopen_file_op = 10;
static int rook_on_semiopen_file_eg = 10;

static int rook_on_semiopen_file_near_king_op = 15;
static int rook_on_semiopen_file_before_king_op = 20;

static int rook_on_open_file_op = 15;
static int rook_on_open_file_eg = 15;

static int rook_on_seven_rank_op = 20;
static int rook_on_seven_rank_eg = 20;

static int rook_mobility_op[] = {
	-8,  -3,  0, 1, 2, 3, 5,  7, 10, 12, 14, 15, 16, 17, 18, 18
};
static int rook_mobility_eg[] = {
	-16, -8, -2, 0, 2, 5, 9, 13, 17, 21, 25, 29, 32, 34, 35, 36
};

static void 
evaluate_rooks(int side)
{
	int xk;
	int file;
	int square;
	int xside;
	int mobility;
	uint64_t my_rooks;
	uint64_t attacks;
	uint64_t mobility_xmask;

	my_rooks = ROOKS(side);
	xside = 1 ^ side;		/* opponent's color */
	xk = brd.kings[xside];	/* location of opponent's king */
	
	if (my_rooks)
		/* exclude these squares from mobility */
		mobility_xmask = ~(side_boards[side] | (pawn_attacks[xside] & ~complete));

	while (my_rooks) {
		square = bit_scan_clear(&my_rooks);
		file = _FILE(square);

		if (!(PAWNS(side) & file_bit[file])) {
			/* there are no our pawns on the rook's file */
			if (PAWNS(xside) & file_bit[file]) {
				/* ... but there are opponent's pawns */
				if (file == (_FILE(xk) - 1) || file == (_FILE(xk) + 1))
					/* opponent's king is on the adjacent file */
					position_score[OPENING][side] += rook_on_semiopen_file_near_king_op;
				else if (file == _FILE(xk))
					/* opponent's king is on the same file */
					position_score[OPENING][side] += rook_on_semiopen_file_before_king_op;
				else {
					/* rook on semiopen file */
					position_score[OPENING][side] += rook_on_semiopen_file_op;
					position_score[ENDGAME][side] += rook_on_semiopen_file_eg;
				}
			} else {
				/* rook on open file */
				position_score[OPENING][side] += rook_on_open_file_op;
				position_score[ENDGAME][side] += rook_on_open_file_eg;
			}
		}
		if (rank_flip[side][_RANK(square)] == 6) {
			/* rook on 7th rank */
			position_score[OPENING][side] += rook_on_seven_rank_op;
			position_score[ENDGAME][side] += rook_on_seven_rank_eg;
		}

		attacks = ROOK_ATTACKS(square, complete);
		/* calculate mobility */
		mobility = popcount(attacks & mobility_xmask);
		mobility_score[OPENING][side] += rook_mobility_op[mobility];
		mobility_score[ENDGAME][side] += rook_mobility_eg[mobility];

		/* king attacks */
		if (attacks & (piece_moves[KING][xk] | BIT(xk))) {
			king_attacks[side] += 2;
			king_attackers[side]++;
		}
	}
}

/* queen evaluation */
static void 
evaluate_queens(int side)
{
	int xk;
	int square;
	uint64_t attacks;
	uint64_t my_queens;

	my_queens = QUEENS(side);
	xk = brd.kings[1 ^ side];

	while (my_queens) {
		square = bit_scan_clear(&my_queens);
		/* king attacks */
		attacks = QUEEN_ATTACKS(square, complete);
		if (attacks & (piece_moves[KING][xk] | BIT(xk))) {
			king_attacks[side] += 4;
			king_attackers[side]++;
		}
	}
}

static int king_square_op[2][64] = {
	{
		 40,  50,  30,	10,  10,  30,  50,	40,
		 30,  40,  20,	 0,   0,  20,  40,	30,
		 10,  20,	0, -20, -20,   0,  20,	10,
		 0,   10, -10, -30, -30, -10,  10,	 0,
		-10,   0, -20, -40, -40, -20,	0, -10,
		-20, -10, -30, -50, -50, -30, -10, -20,
		-30, -20, -40, -60, -60, -40, -20, -30,
		-40, -30, -50, -70, -70, -50, -30, -40
	},
	{
		-40, -30, -50, -70, -70, -50, -30, -40,
		-30, -20, -40, -60, -60, -40, -20, -30,
		-20, -10, -30, -50, -50, -30, -10, -20,
		-10,   0, -20, -40, -40, -20,	0, -10,
		 0,   10, -10, -30, -30, -10,  10,	 0,
		 10,  20,	0, -20, -20,   0,  20,	10,
		 30,  40,  20,	 0,   0,  20,  40,	30,
		 40,  50,  30,	10,  10,  30,  50,	40
	}
};

static int king_square_eg[64] = {
	-72, -48, -36, -24, -24, -36, -48, -72,
	-48, -24, -12,	 0,   0, -12, -24, -48,
	-36, -12,	0,	12,  12,   0, -12, -36,
	-24,   0,  12,	24,  24,  12,	0, -24,
	-24,   0,  12,	24,  24,  12,	0, -24,
	-36, -12,	0,	12,  12,   0, -12, -36,
	-48, -24, -12,	 0,   0, -12, -24, -48,
	-72, -48, -36, -24, -24, -36, -48, -72
};

static int attack_penalty_op = -20;
static int attack_scale[16] = {
	0, 0, 8, 10, 14, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16
};

static const int side_offset[2] = {-8, 8};

/* evaluate king */
static void 
evaluate_king(int side)
{
	int i;
	int file;
	int rank;
	int square;
	int xside;
	uint64_t bb, xbb;

	xside = 1 ^ side;			/* opponent's side */
	square = brd.kings[side];

	file = _FILE(square);
	rank = _RANK(square);

	/* include adjacent pawns into mask */
	if ((side == WHITE && rank != 0) || (side == BLACK && rank != 7))
		i = square + side_offset[side];
	else
		i = square;

	/* pawn shield and pawn attacks */
	bb = passer_mask[side][i] & PAWNS(side);
	xbb = passer_mask[side][i] & PAWNS(xside);

	if (file != 0) {
		/* in case if there is no our pawn */
		king_score[OPENING][side] += -36;

		if (bb & file_bit[file - 1]) {
			/* find nearest pawn */
			if (side == BLACK)
				i = bit_scan_rev(bb & file_bit[file - 1]);
			else
				i = bit_scan(bb & file_bit[file - 1]);
			king_score[OPENING][side] += (7 - rank_flip[side][_RANK(i)]) * \
									(7 - rank_flip[side][_RANK(i)]);
		}
	}

	/* in case if there is no our pawn in front of our king */
	king_score[OPENING][side] += -36;

	if (bb & file_bit[file]) {
		if (side == BLACK)
			i = bit_scan_rev(bb & file_bit[file]);
		else
			i = bit_scan(bb & file_bit[file]);
		king_score[OPENING][side] += (7 - rank_flip[side][_RANK(i)]) * \
								(7 - rank_flip[side][_RANK(i)]);
	}

	if (file != 7) {
		/* in case if there is no our pawn */
		king_score[OPENING][side] += -36;
		if (bb & file_bit[file + 1]) {
			/* find nearest pawn */
			if (side)
				i = bit_scan_rev(bb & file_bit[file + 1]);
			else
				i = bit_scan(bb & file_bit[file + 1]);
			king_score[OPENING][side] += (7 - rank_flip[side][_RANK(i)]) * \
									(7 - rank_flip[side][_RANK(i)]);
		}
	}

	king_score[OPENING][xside] += (attack_penalty_op * king_attacks[side] *
		attack_scale[king_attackers[side]]) / 16;

	position_score[OPENING][side] += king_square_op[side][square];
	position_score[ENDGAME][side] += king_square_eg[square];
}

static int blocked_rook = -50;
static int trapped_bishop = -100;

static void 
evaluate_blocked_pieces(int side)
{
	int xside = 1 ^ side;
	int side_inc = side * (A8 - A1);

	/* rook */
	if (square64[F1 + side_inc] == KING || square64[G1 + side_inc] == KING) {
		if (square64[G1 + side_inc] == ROOK || square64[H1 + side_inc] == ROOK)
			position_score[OPENING][side] += blocked_rook;
	} else if (square64[C1 + side_inc] == KING || square64[B1 + side_inc] == KING) {
		if (square64[A1 + side_inc] == ROOK || square64[B1 + side_inc] == ROOK)
			position_score[OPENING][side] += blocked_rook;
	}

	int b_sq_a7 = side == WHITE? A7 : A2;
	int b_sq_h7 = side == WHITE? H7 : H2;
	int p_sq_b6 = side == WHITE? B6 : B3;
	int p_sq_g6 = side == WHITE? G6 : G3;

	if ((BISHOPS(side) & BIT(b_sq_a7)) && (PAWNS(xside) & BIT(p_sq_b6)) && \
			SEE(SET_FROM(b_sq_a7) | SET_TO(p_sq_b6) | SET_TYPE(TYPE_CAPTURE)) < 0) {
		position_score[OPENING][side] += trapped_bishop;
		position_score[ENDGAME][side] += trapped_bishop;
	}
	if ((BISHOPS(side) & BIT(b_sq_h7)) && (PAWNS(xside) & BIT(p_sq_g6)) && \
			SEE(SET_FROM(b_sq_h7) | SET_TO(p_sq_g6) | SET_TYPE(TYPE_CAPTURE)) < 0) {
		position_score[OPENING][side] += trapped_bishop;
		position_score[ENDGAME][side] += trapped_bishop;
	}
}

/* clear scores */
static void 
zero_scores(void)
{
	king_attacks[WHITE] = 0;
	king_attacks[BLACK] = 0;
	king_attackers[WHITE] = 0;
	king_attackers[BLACK] = 0;

	king_score[OPENING][WHITE] = 0;
	king_score[OPENING][BLACK] = 0;
	king_score[ENDGAME][WHITE] = 0;
	king_score[ENDGAME][BLACK] = 0;

	mobility_score[OPENING][WHITE] = 0;
	mobility_score[OPENING][BLACK] = 0;
	mobility_score[ENDGAME][WHITE] = 0;
	mobility_score[ENDGAME][BLACK] = 0;

	pawn_score[OPENING][WHITE] = 0;
	pawn_score[OPENING][BLACK] = 0;
	pawn_score[ENDGAME][WHITE] = 0;
	pawn_score[ENDGAME][BLACK] = 0;

	position_score[OPENING][WHITE] = 0;
	position_score[OPENING][BLACK] = 0;
	position_score[ENDGAME][WHITE] = 0;
	position_score[ENDGAME][BLACK] = 0;
}

static int simple_mate_squares[2][64] = {
	{ 
		160, 140, 120, 100, 100, 120, 140, 160,
		140, 120, 100,	80,  80, 100, 120, 140,
		120, 100,  80,	60,  60,  80, 100, 120,
		100,  80,  60,	40,  40,  60,  80, 100,
		100,  80,  60,	40,  40,  60,  80, 100,
		120, 100,  80,	60,  60,  80, 100, 120,
		140, 120, 100,	80,  80, 100, 120, 140,
		160, 140, 120, 100, 100, 120, 140, 160
	},
	{ 
		160, 140, 120, 100, 100, 120, 140, 160,
		140, 120, 100,	80,  80, 100, 120, 140,
		120, 100,  80,	60,  60,  80, 100, 120,
		100,  80,  60,	40,  40,  60,  80, 100,
		100,  80,  60,	40,  40,  60,  80, 100,
		120, 100,  80,	60,  60,  80, 100, 120,
		140, 120, 100,	80,  80, 100, 120, 140,
		160, 140, 120, 100, 100, 120, 140, 160
	}
};

/* for KING vs KING, KNIGHT, BISHOP */
static int b_n_mate_dark_squares[64] = {
	99, 90, 80, 70, 60, 50, 40, 30,
	90, 80, 70, 60, 50, 40, 30, 40,
	80, 70, 60, 50, 40, 30, 40, 50,
	70, 60, 50, 40, 30, 40, 50, 60,
	60, 50, 40, 30, 40, 50, 60, 70,
	50, 40, 30, 40, 50, 60, 70, 80,
	40, 30, 40, 50, 60, 70, 80, 90,
	30, 40, 50, 60, 70, 80, 90, 99
};

static int b_n_mate_light_squares[64] = {
	30, 40, 50, 60, 70, 80, 90, 99,
	40, 30, 40, 50, 60, 70, 80, 90,
	50, 40, 30, 40, 50, 60, 70, 80,
	60, 50, 40, 30, 40, 50, 60, 70,
	70, 60, 50, 40, 30, 40, 50, 60,
	80, 70, 60, 50, 40, 30, 40, 50,
	90, 80, 70, 60, 50, 40, 30, 40,
	99, 90, 80, 70, 60, 50, 40, 30
};

static int 
evaluate_mate(int side)
{
	int score = 0;
	int xside = 1 ^ side;

	if (material_complete[side]) {
		score = material_complete[side] + 200;

		if (piece_count[side][KNIGHT] == 1 && piece_count[side][BISHOP] == 1) {
			if (BISHOPS(side) & BLACKSQUARES)
				score += b_n_mate_dark_squares[brd.kings[xside]];
			else
				score += b_n_mate_light_squares[brd.kings[xside]];
		} else {
			score += simple_mate_squares[side][brd.kings[xside]];
			score -= distance[brd.kings[side]][brd.kings[xside]] * 10;
		}
	}
	return score;
}

static int evaluate_draw_pattern()
{
	/* TODO: add real draw code */
	return FALSE;
}

static int bishop_pair = 50;
static int bishop_pair_correction_more = 5;
static int bishop_pair_correction_less = 2;

/* based on Larry Kaufman's paper */
static void 
evaluate_material_imbalances(int side)
{
	/* TODO: more imbalances */

	int pc;

	/* bishop pair */
	if (piece_count[side][BISHOP] > 1) {
		side_score[OPENING][side] += bishop_pair;
		side_score[ENDGAME][side] += bishop_pair;

		/* how many pawns on the board? */
		pc = wpc + bpc;

		if (pc > 12) {
			side_score[OPENING][side] -= (4 - (16 - pc)) * bishop_pair_correction_more;
			side_score[ENDGAME][side] -= (4 - (16 - pc)) * bishop_pair_correction_more;
		} else if (pc < 8) {
			side_score[OPENING][side] += (8 - pc) * bishop_pair_correction_less;
			side_score[ENDGAME][side] += (8 - pc) * bishop_pair_correction_less;
		}
	}
}
