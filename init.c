/*
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#include "common.h"
#include "board.h"
#include "init.h"
#include "init_attacks.h"

/* classic mailbox board representation */
const int mailbox[120] = {
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 
	-1,  0,  1,  2,  3,  4,  5,  6,  7, -1, 
	-1,  8,  9, 10, 11, 12, 13, 14, 15, -1, 
	-1, 16, 17, 18, 19, 20, 21, 22, 23, -1, 
	-1, 24, 25, 26, 27, 28, 29, 30, 31, -1, 
	-1, 32, 33, 34, 35, 36, 37, 38, 39, -1, 
	-1, 40, 41, 42, 43, 44, 45, 46, 47, -1, 
	-1, 48, 49, 50, 51, 52, 53, 54, 55, -1, 
	-1, 56, 57, 58, 59, 60, 61, 62, 63, -1, 
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1	
};

const int mailbox64[64] = {
	21, 22, 23, 24, 25, 26, 27, 28,
	31, 32, 33, 34, 35, 36, 37, 38,
	41, 42, 43, 44, 45, 46, 47, 48,
	51, 52, 53, 54, 55, 56, 57, 58,
	61, 62, 63, 64, 65, 66, 67, 68,
	71, 72, 73, 74, 75, 76, 77, 78,
	81, 82, 83, 84, 85, 86, 87, 88,
	91, 92, 93, 94, 95, 96, 97, 98 };

const int pieces_offsets[7][8] = {
	{	0,	 0,   0,  0, 0,  0,  0,  0 },
	{	0,	 0,   0,  0, 0,  0,  0,  0 },
	{ -21, -19, -12, -8, 8, 12, 19, 21 },
	{ -11,	-9,   9, 11, 0,  0,  0,  0 },
	{ -10,	-1,   1, 10, 0,  0,  0,  0 },
	{ -11, -10,  -9, -1, 1,  9, 10, 11 },
	{ -11, -10,  -9, -1, 1,  9, 10, 11 }
};

const int offsets[7] = { 0, 0, 8, 4, 4, 8, 8 };

void 
initialize_bitboards(void)
{
	initialize_bitcount();
	initialize_lzarray();
	initialize_moves();
	initialize_forward_ray();
	initialize_passers();
	initialize_isolated();
	initialize_distance();
	initialize_attacks();
	initialize_king_shield();
}

void 
initialize_bitcount(void)
{
	int i, j, n;

	bitcount[0] = 0;
	bitcount[1] = 1; 
	i = 1;
	
	for (n = 2; n <= 16; n++) {
		i <<= 1;
		for (j = i; j <= i + (i-1); j++)
			bitcount[j] = 1 + bitcount[j - i];
	}
}

void 
initialize_lzarray(void)
{
	int i, j, s, n;

	s = n = 1;
	for (i = 0; i < 16; i++) {
		for (j = s; j < s + n; j++)
			lzArray[j] = 16 - 1 - i;
		s += n;
		n += n;
	}
}

void 
initialize_moves(void)
{
	initialize_pawn_moves();
	initialize_piece_moves();
}

void 
initialize_pawn_moves(void)
{
	int i;
	int side;
	int sign_side[2] = { 1, -1 };

	ZERO_MEM(pawn_moves);
	for (side = WHITE; side <= BLACK; side++) {
		for (i = 0; i < 64; i++) {
			if (_FILE(i) != 7)
				SET(pawn_moves[side][i], i + sign_side[side] * (9 - side * 2));
			if (_FILE(i) != 0)
				SET(pawn_moves[side][i], i + sign_side[side] * (7 + side * 2));
		}
	}
}

void 
initialize_piece_moves(void)
{
	int i, j, x;
	int piece;

	ZERO_MEM(piece_moves);
	for (piece = KNIGHT; piece <= KING; piece++)
		for (i = 0; i < 64; i++)
			for (j = 0; j < offsets[piece]; j++) {
				x = i;
				while (TRUE) {
					x = mailbox[mailbox64[x] + pieces_offsets[piece][j]];
					if (x != -1)
						SET(piece_moves[piece][i], x);
					if (x == -1 || piece == KNIGHT || piece == KING)
						break;
				}
			}
}

void 
initialize_forward_ray(void)
{
	int i, j, x;
	uint64_t prev;

	ZERO_MEM(forward_ray);

	for (i = 0; i < 64; i++) {
		for (j = WHITE; j <= BLACK; j++) {
			x = i;
			prev = 0;
			while (TRUE) {
				x = mailbox[mailbox64[x] + 10 * (j ? -1 : 1)];
				
				if (x == -1)
					break;

				SET(forward_ray[j][i], x);
				forward_ray[j][i] |= prev;
				prev = forward_ray[j][i];
			}
		}
	}
}

void 
initialize_passers(void)
{
	int i, side;

	ZERO_MEM(passer_mask);

	for (side = 0; side <= 1; side++)
		for (i = 0; i < 64; i++) {
			passer_mask[side][i] = forward_ray[side][i];
			if (_FILE(i) != 0)
				passer_mask[side][i] |= forward_ray[side][i - 1];
			if (_FILE(i) != 7)
				passer_mask[side][i] |= forward_ray[side][i + 1];
		}
}

void 
initialize_isolated(void)
{
	isolated_mask[0] = file_bit[1];
	isolated_mask[1] = file_bit[0] | file_bit[2];
	isolated_mask[2] = file_bit[1] | file_bit[3];
	isolated_mask[3] = file_bit[2] | file_bit[4];
	isolated_mask[4] = file_bit[3] | file_bit[5];
	isolated_mask[5] = file_bit[4] | file_bit[6];
	isolated_mask[6] = file_bit[5] | file_bit[7];
	isolated_mask[7] = file_bit[6];
}

void 
initialize_distance(void)
{
	int from, to;
	int file, rank;

	for (from = 0; from < 64; from++)
		for (to = 0; to < 64; to++) {
			file = abs(_FILE(from) - _FILE(to));
			rank = abs(_RANK(from) - _RANK(to));
			distance[from][to] = MAX(file, rank);
		}
}

void 
initialize_king_shield(void)
{
	int side, square;

	ZERO_MEM(king_shield);
	for (side = 0; side <= 1; side++)
		for (square = 0; square < 64; square++) {
			king_shield[side][square] |= file_bit[_FILE(square)];
			if (_FILE(square) != 0)
				king_shield[side][square] |= file_bit[_FILE(square - 1)];
			if (_FILE(square) != 7)
				king_shield[side][square] |= file_bit[_FILE(square + 1)];
		}
}
