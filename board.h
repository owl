/*
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#define WHITE_CASTLE_KINGSIDE	 (1 << 0)
#define WHITE_CASTLE_QUEENSIDE	 (1 << 1)
#define BLACK_CASTLE_KINGSIDE	 (1 << 2)
#define BLACK_CASTLE_QUEENSIDE	 (1 << 3)

struct board_t {
	int cap_piece;				/* if we are capturing piece now */
	int castle_mask;			/* a mask of allowed castle moves */
	int ep;						/* enpassant target */
	int kings[2];				/* kings locations */
	int wtm;					/* side to move */
	int fifty_rule;				/* number of moves after last capture or pawn
								   move */
	int move;					/* last move */
	uint64_t hash_key;			/* zobrist key */
	uint64_t hash_pawn_key;
};

struct game_history_t {
	int count;
	struct board_t board[1024];
};

/* see board.c for comments */
extern const char *char_board[64];
extern const char char_pieces[7];

extern int bitcount[65536];
extern int lzArray[65536];
extern uint64_t isolated_mask[8];
extern uint64_t passer_mask[2][64];
extern uint64_t pawn_moves[2][64]; 
extern uint64_t piece_moves[7][64];
extern uint64_t forward_ray[2][64];
extern uint64_t king_shield[2][64];

extern int distance[64][64];
extern const uint64_t file_bit[8];

extern int square64[64];
extern struct board_t brd;
extern struct game_history_t game_history;
extern uint64_t complete;

extern uint64_t piece_boards[2][7];
extern uint64_t side_boards[2];

extern int castled[2];

extern int material_complete[2];
extern int material_pawns[2];
extern int material_pieces[2];

extern int piece_value[7];
extern int piece_count[2][7];

/* functions */
void add_piece(int square, int piece, int side);
void calc_zobrist(uint64_t *hash_key, uint64_t *hash_pawn_key);
char *current_fen_position(char *buf);
void remove_piece(int square, int piece, int side);
int reps(void);
int SEE(int move);
int setup_board(char *fen);
void update_boards(void);

/* debuging functions */
int board_is_legal(struct board_t *b);
void print_bb(uint64_t b);
void print_board(void);

/* some defines for simplicity only */
#define PAWNS(side)		(piece_boards[side][PAWN])
#define KNIGHTS(side)	(piece_boards[side][KNIGHT])
#define BISHOPS(side)	(piece_boards[side][BISHOP])
#define ROOKS(side)		(piece_boards[side][ROOK])
#define QUEENS(side)	(piece_boards[side][QUEEN])
#define KING(side)		(piece_boards[side][KING])

#define wpc		piece_count[WHITE][PAWN]
#define bpc		piece_count[BLACK][PAWN]
#define wnc		piece_count[WHITE][KNIGHT]
#define bnc		piece_count[BLACK][KNIGHT]
#define wbc		piece_count[WHITE][BISHOP]
#define bbc		piece_count[BLACK][BISHOP]
#define wrc		piece_count[WHITE][ROOK]
#define brc		piece_count[BLACK][ROOK]
#define wqc		piece_count[WHITE][QUEEN]
#define bqc		piece_count[BLACK][QUEEN]

/* evaluation of the position for material */
#define MATERIAL_SCORE	(material_complete[brd.wtm] - material_complete[1 ^ brd.wtm])
