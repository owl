/*
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#include "common.h"
#include "attacks.h"
#include "board.h"
#include "engine.h"
#include "move.h"

#define DEF_LMR_EXTEND_CHECK 1
#define DEF_LMR_REDUCE -1

/* check if we should extend or can reduce search */
int 
get_extensions(int move, int in_check, int move_is_check, int depth, int ply)
{
	assert(move);
	assert(in_check == TRUE || in_check == FALSE);
	assert(move_is_check == TRUE || move_is_check == FALSE);
	assert(depth >= 0 && depth <= MAX_PLY);
	assert(ply >= 0 && ply <= MAX_PLY);

	/* extend if we are in check */
	if (in_check)
		return DEF_LMR_EXTEND_CHECK;

	/* don't reduce checks, tactical moves, killers, moves from transposition
	   table and all moves at low depth */
	if (!e.lmr || move_is_check || depth < 3 || MOVE_IS_TACTICAL(move) || \
			move == killers[ply].killer1 || move == killers[ply].killer2 || \
			move == killers[ply].mate_killer || move == hash_moves[ply])
		return 0;

	/* don't reduce passed pawn moves */
	if (square64[GET_TO(move)] == PAWN)
		if (!(PAWNS(brd.wtm) & passer_mask[1 ^ brd.wtm][GET_TO(move)]))
			return 0;

#ifdef STATISTIC_COUNTERS
	counters.reductions++;
#endif
	/* reduce search depth */
	return DEF_LMR_REDUCE;
}
