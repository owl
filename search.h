/*
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
extern int follow_pv;
extern int abort_search;
extern int fail_high_root;
extern int leave_book;
extern int ponder_move;
extern int safe_search;

int get_extensions(int move, int in_check, int is_check, int depth, int ply);
void history_store(int move, int depth, int ply, int score);
int iterate(char *ponder_candidate);
int quiesce(int alpha, int beta, int depth, int ply, int flag);
int search_root(int alpha, int beta, int depth, int check);
int search_pv(int alpha, int beta, int depth, int ply, int check);
int zwsearch(int beta, int depth, int ply, int check, int do_null);
