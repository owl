/*
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#include "common.h"
#include "attacks.h"
#include "board.h"
#include "move.h"

int SEE(int move)
{
	int count;
	int square;
	int from, to, type;
	int piece;
	int att_side, def_side;
	int last_val;
	uint64_t bb;
	uint64_t occ;
	uint64_t atts, defs;
	int swap_list[32];

	occ = complete;
	from = GET_FROM(move);
	to = GET_TO(move);
	type = GET_TYPE(move);

	att_side = (BIT(from) & side_boards[WHITE]) ? WHITE : BLACK;
	def_side = 1 ^ att_side;

	CLEAR(occ, from);
	atts = ATTACK_TO(to, att_side, occ);
	defs = ATTACK_TO(to, def_side, occ);
	CLEAR(atts, from);

	swap_list[0] = 0;
	last_val = 0;

	if (type & TYPE_PROMOTE) {
		swap_list[0] = piece_value[GET_PROMOTE(move)] - piece_value[PAWN];
		last_val = -piece_value[GET_PROMOTE(move)];
	}
	swap_list[0] += (GET_TYPE(move) & TYPE_ENPASSANT) ? piece_value[PAWN] : piece_value[square64[to]];
	last_val += -piece_value[square64[from]];

	count = 1;
	while (defs) {
		for (piece = PAWN; piece <= KING; piece++) {
			bb = defs & piece_boards[def_side][piece];
			if (bb) {
				square = bit_scan(bb);
				defs &= defs - 1;

				swap_list[count] = swap_list[count - 1] + last_val;
				last_val = piece_value[piece];
				count++;

				break;
			}
		}
		if (!atts) {
			break;
		}
		for (piece = PAWN; piece <= KING; piece++) {
			bb = atts & piece_boards[att_side][piece];
			if (bb) {
				square = bit_scan(bb);
				atts &= atts - 1;

				swap_list[count] = swap_list[count - 1] + last_val;
				last_val = -piece_value[piece];
				count++;

				break;
			}
		}

	}

	count--;

	while (count) {
		if (count & 1) {
			if (swap_list[count] <= swap_list[count - 1])
				swap_list[count - 1] = swap_list[count];
		} else {
			if (swap_list[count] >= swap_list[count - 1])
				swap_list[count - 1] = swap_list[count];
		}
		count--;
	}

	return swap_list[0];
}
