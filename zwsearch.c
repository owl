/*
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#include "common.h"
#include "attacks.h"
#include "board.h"
#include "engine.h"
#include "evaluate.h"
#include "move.h"
#include "search.h"
#include "trans.h"
#include <math.h>

static int futility_margins[] = {0, 125, 175, 325, 375, 425};

/* zero window search */
int 
zwsearch(int beta, int depth, int ply, int check, int do_null)
{
	int i;
	int extend;
	int moves_count;
	int score;
	int futility_max;
	int futility_prune;
	int is_check;
	struct moves_t moves;

	assert(beta > -MATE_VALUE && beta <= +MATE_VALUE);
	assert(depth >= 0 && depth <= MAX_PLY);
	assert(board_is_legal(&brd));

	if (check_time(ply)) 
		return 65535;

	if (reps() || evaluate_draw())
		return 0;

	/* if we are too deep */
	if (ply >= MAX_PLY - 1)
		return evaluate(beta - 1, beta);

	/* search for captures */
	if (depth <= 0 && !check)
		return quiesce(beta - 1, beta, 0, ply, 0);

	futility_prune = FALSE;
	futility_max = 0;
	hash_moves[ply] = 0;

	/* check transposition table */
	if (e.main_hash_enabled) {
		i = hashtable_get(brd.wtm, depth, ply, &hash_moves[ply], &score);
		if (i) {
#ifdef STATISTIC_COUNTERS
			counters.transposition++;
#endif
			switch (i) {
				case EXACT:
					return score;
				case LOWER:
					if (score >= beta)
						return beta;
					do_null = FALSE;
					break;
				case UPPER:
					if (score < beta)
						return beta - 1;
					break;
				default:
					assert(0);
			}
		}
	}

	/* NULL move pruning */
	if (e.null_pruning && do_null && !check && (depth >= 2) && \
			(MATERIAL_SCORE >= beta - 875) && \
			!SCORE_IS_MATE(beta) && material_pieces[brd.wtm]) {

		int null_reduction = depth >= 5? 3 : 2;

		brd.cap_piece = 0;
		brd.move = 0;			/* NULL move */

		/* for takeback */
		memcpy(&game_history.board[game_history.count++], &brd, \
				sizeof(struct board_t));

		if (brd.ep != -1)
			brd.hash_key ^= hash_ep[_FILE(brd.ep)];
		brd.hash_key ^= hash_side;

		brd.fifty_rule = 0;
		brd.ep = -1; 
		brd.wtm ^= 1;

		/* search reduced depth */
		if (depth - null_reduction - 1 > 0) {
			score = -zwsearch(1 - beta, depth - null_reduction - 1, \
					ply + 1, 0, 0);
			if (abort_search) 
				return 65535;
		} else {
			score = -quiesce(-beta, 1 - beta, 0, ply + 1, 1);
			if (abort_search) 
				return 65535;
		}

		takeback();

		if (score >= beta) {
#ifdef STATISTIC_COUNTERS
			counters.null_move_prunings++;
#endif
			return beta;
		}
	} 

	/* extended futility pruning */
	if (e.futility_pruning && depth <= 5 && !check && !SCORE_IS_MATE(beta)) {
		assert(depth > 0);

		if (MATERIAL_SCORE + futility_margins[depth] < beta)
			futility_prune = TRUE;
	}
	moves_count = 0;
	gen_moves(&moves);

	for (i = 0; i < moves.count; i++) {
		/* get move with largest score */
		sort_moves(&moves, i, ply);
		if (!make_move(moves.move[i], FALSE))
			continue;
		
		is_check = IS_CHECKED(brd.wtm);

		if (futility_prune && !is_check && moves_count && !MOVE_IS_TACTICAL(moves.move[i])) {
			takeback();
#ifdef STATISTIC_COUNTERS
			counters.futility_prunings++;
#endif
			continue;
		}

		moves_count++;

		/* examine if we can reduce or extend move */
		extend = moves_count > NOLMR_MOVES? \
			get_extensions(moves.move[i], check, is_check, depth, ply) : \
			check;
		if (extend < 0) {
			extend = MIN(-MIN(depth - 2, (moves_count > 2)? 2 : 1), 0);
		}

		score = -zwsearch(1 - beta, depth + extend - 1, ply + 1, is_check, 1);
		if (abort_search) 
			return 65535;

		if (score >= beta && extend < 0) {
			/* research with original depth */
			score = -zwsearch(1 - beta, depth - 1, ply + 1, is_check, 1);
			if (abort_search) 
				return 65535;
		}

		takeback();

			if (score >= beta) {
#ifdef STATISTIC_COUNTERS
				counters.failed_high_total++;
				if (moves_count == 1) 
					counters.failed_high_first++;
#endif
				/* update history only for non tactical move */
				if (!MOVE_IS_TACTICAL(moves.move[i]))
					history_store(moves.move[i], depth, ply, beta);
				if (e.main_hash_enabled)
					/* save in hashtable */
					hashtable_put(brd.wtm, depth, ply, LOWER, moves.move[i],
							beta);

				return beta;
			} 
	}
	if (!moves_count)
		beta = check? -MATE_VALUE + ply + 1 : 1;

	if (e.main_hash_enabled)
		/* save in hashtable */
		hashtable_put(brd.wtm, depth, ply, UPPER, 0, beta - 1);

	return beta - 1;
}
