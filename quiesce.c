/*
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#include <assert.h>
#include <stdio.h>
#include "common.h"
#include "attacks.h"
#include "board.h"
#include "engine.h"
#include "evaluate.h"
#include "move.h"
#include "search.h"

int quiesce(int alpha, int beta, int depth, int ply, int flag)
{
	int i, j;
	int is_pv;
	int score;
	int check;
	int moves_count;
	uint64_t target;
	struct moves_t moves;

	assert(alpha >= -MATE_VALUE && alpha < +MATE_VALUE);
	assert(beta > alpha && beta <= +MATE_VALUE);
	assert(board_is_legal(&brd));

	if (evaluate_draw())
		return 0;

	is_pv = (alpha != beta - 1);
	/* don't store PV at non-PV nodes */
	if (is_pv)
		pv_length[ply] = ply;

	check = IS_CHECKED(brd.wtm);
	if (check) {
		if (is_pv)
			return search_pv(alpha, beta, 0, ply, TRUE);
		else
			return zwsearch(beta, 0, ply, TRUE, 1);
	}

	target = side_boards[1 ^ brd.wtm];

	/* stand pat */
	score = evaluate(alpha, beta);

	/* already got cutoff */
	if (score >= beta)
		return beta;

	/* we are too deep */
	if (ply >= MAX_PLY - 1)
		return score;

	if (score > alpha)
		alpha = score;
	else if (e.delta_pruning && !check && !is_pv && !flag && score < (alpha - 250)) {
		target &= ~PAWNS(1 ^ brd.wtm);

		if (score < (alpha - 475)) {
			target &= ~KNIGHTS(1 ^ brd.wtm);
			target &= ~BISHOPS(1 ^ brd.wtm);
		}

		if (score < (alpha - 650))
			target &= ~ROOKS(1 ^ brd.wtm);

		if (score < (alpha - 1125))
			target &= ~QUEENS(1 ^ brd.wtm);
	}
	gen_quiescent_moves(&moves, target);

	moves_count = 0;

	/* don't use hash move and killers in QS */
	hash_moves[ply] = 0;
	killers[ply].mate_killer = 0;
	killers[ply].killer1 = 0;
	killers[ply].killer2 = 0;

	for (i = 0; i < moves.count; i++) {
		assert(MOVE_IS_TACTICAL(moves.move[i]));
		sort_moves(&moves, i, ply);

		if (e.delta_pruning && moves_count && !flag && !is_pv && !check && MOVE_IS_TACTICAL(moves.move[i]) && \
				!SCORE_IS_MATE(beta) && moves.see[i] < 0) {
#ifdef STATISTIC_COUNTERS
			counters.delta_prunings++;
#endif
			continue;
		}

		if (!make_move(moves.move[i], FALSE))
			continue;

		moves_count++;

		score = -quiesce(-beta, -alpha, depth - 1, ply + 1, flag);
		if (abort_search) 
			return 65535;

		takeback();

		if (score > alpha) {
			if (score >= beta) {
#ifdef STATISTIC_COUNTERS
				counters.failed_high_total++;
				if (moves_count == 1)
					counters.failed_high_first++;
#endif

				return beta;
			}
			alpha = score;

			if (is_pv) {
				pv[ply][ply] = moves.move[i];

				for (j = ply + 1; j < pv_length[ply + 1]; j++)
					pv[ply][j] = pv[ply + 1][j];
				pv_length[ply] = pv_length[ply + 1];
			}
		}
	}
	return alpha;
}
