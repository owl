/*
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#define TYPE_PAWN_ONE		1
#define TYPE_PAWN_TWO		2
#define TYPE_CAPTURE		4
#define TYPE_PROMOTE		8
#define TYPE_CASTLE		16
#define TYPE_ENPASSANT	32
#define TYPE_TACTICAL	(TYPE_CAPTURE | TYPE_ENPASSANT | TYPE_PROMOTE)

#define SET_FROM(x)			(x)
#define SET_TO(x)			((x) << 6)
#define SET_TYPE(x)			((x) << 12)
#define SET_PROMOTE(x)		((x) << 18)

#define GET_FROM(x)			((x) & 0x3f)
#define GET_TO(x)			(((x) >> 6) & 0x3f)
#define GET_TYPE(x)			(((x) >> 12) & 0x3f)
#define GET_PROMOTE(x)		(((x) >> 18) & 7)

#define MOVE_IS_TACTICAL(x)	(GET_TYPE(x) & TYPE_TACTICAL)
#define PIECE(x)			(square64[GET_FROM(x)])
#define CAP_PIECE(x)			(square64[GET_TO(x)])

#define MAX_MOVES 512

struct moves_t {
	int count;
	int move[MAX_MOVES];
	int score[MAX_MOVES];
	int see[MAX_MOVES];
};

struct parsed_moves_t {
	int count;
	int move[MAX_MOVES];
	char sans[MAX_MOVES][32];
};

void sort_moves(struct moves_t *moves, int current, int ply);
char *coord_move(int move, char *buf);
char *get_pv_string_coord(char * buf);
char *get_pv_string_san(char * buf);
int make_move(int move, int tb);
int move_is_legal(int move);
int parse_move_coord(char * move);
int parse_move_san(char * move);
void print_moves_coord(struct moves_t * moves);
void print_moves_san(struct moves_t *moves);
char *san_move(int move, char * buf);
void takeback(void);
void print_move_coord(int move);

int gen_legal_moves(struct moves_t *moves);
int gen_moves(struct moves_t *moves);
int gen_quiescent_moves(struct moves_t *moves, uint64_t target);
