/*
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#include <assert.h>
#include <stdio.h>
#include "common.h"
#include "attacks.h"
#include "board.h"
#include "engine.h"
#include "move.h"
#include "search.h"

int 
search_root(int alpha, int beta, int depth, int check)
{
	int i, j;
	int extend;
	int score;
	int is_check;
	int moves_count;
	int searching_pv;
	struct moves_t moves;

	assert(alpha >= -MATE_VALUE && alpha < +MATE_VALUE);
	assert(beta > alpha && beta <= +MATE_VALUE);
	assert(depth >= 0 && depth <= MAX_PLY);
	assert(board_is_legal(&brd));

	moves_count = 0;
	searching_pv = TRUE;

	if (reps() >= 3)
		return 0;

	pv_length[0] = 0;
	hash_moves[0] = pv[0][0];

	gen_moves(&moves);

	for (i = 0; i < moves.count; i++) {
		/* get move with largest score */
		sort_moves(&moves, i, 0);

		if (!make_move(moves.move[i], FALSE))
			continue;

		moves_count++;
		fail_high_root = FALSE;

		is_check = IS_CHECKED(brd.wtm);

		if (searching_pv) {
			score = -search_pv(-beta, -alpha, depth + check - 1, 1, is_check);
			if (abort_search) 
				return 65535;
		} else {
			/* examine if we can reduce or extend move */
			extend = moves_count > NOLMR_MOVES? \
					 get_extensions(moves.move[i], check, is_check, depth, 0) : \
					 check;
			if (extend < 0) {
				extend = MIN(-MIN(depth - 2, (moves_count > 2)? 2 : 1), 0);
			}

			score = -zwsearch(-alpha, depth + extend - 1, 1, is_check, 1);
			if (abort_search) 
				return 65535;

			if (score > alpha && extend < 0) {
				score = -zwsearch(-alpha, depth - 1, 1, is_check, 1);
				if (abort_search) 
					return 65535;
			}

			if (score > alpha && score < beta) {
				fail_high_root = TRUE;
				score = -search_pv(-beta, -alpha, depth + check - 1, 1, is_check);
				if (abort_search) 
					return 65535;
			}
		}

		takeback();

		searching_pv = FALSE;

		if (score > alpha) {
			if (score >= beta) {
#ifdef STATISTIC_COUNTERS
				counters.failed_high_total++;
				if (moves_count == 1)
					counters.failed_high_first++;
#endif
				if (!MOVE_IS_TACTICAL(moves.move[i]))
					history_store(moves.move[i], depth, 0, beta);

				return beta;
			}
			alpha = score;
			pv[0][0] = moves.move[i];

			for (j = 1; j < pv_length[1]; j++)
				pv[0][j] = pv[1][j];
			pv_length[0] = pv_length[1];
		}
	}

	if (!moves_count && !check)
		return 0;

	return alpha;
}
