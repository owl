/*
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef NOLMR_MOVES
#define NOLMR_MOVES 3
#endif

/* killer heuristics */
struct killers_t {
	int killer1;
	int killer2;
	int mate_killer;
};

struct counters_t {
	uint64_t delta_prunings;
	uint64_t evaluations;
	uint64_t lazy_evaluations;
	uint64_t failed_high_first;
	uint64_t failed_high_total;
	uint64_t futility_prunings;
	uint64_t null_move_prunings;
	uint64_t misc;
	uint64_t pawn_hash_evaluations;
	uint64_t razorings;
	uint64_t reductions;
	uint64_t searched_nodes;
	uint64_t transposition;
};

extern int history[2][7][4096];				/* cut off history */
extern int hash_moves[MAX_PLY];				/* moves from transposition
											   table (searched first) */
extern struct killers_t killers[MAX_PLY];
extern struct counters_t counters;

extern int pv[MAX_PLY][MAX_PLY];
extern int pv_length[MAX_PLY];
extern char return_ponder[32];

struct epd_info_t {
	char *filename;		/* filename of .epd file. we should free this pointer
						   after completing tests */
	int think_time;		/* time in seconds for each position */
	int max_tests;		/* max number of tests to do */
	int first_test;		/* start from */
};

struct engine_t {
	int side;			/* what engine's side */
	int phase_factor;

	int force_mode;
	int post_mode;
	int xboard_mode;
	int play_computer;	/* playing with a computer engine */
	int san_notation;	/* output thinking using SAN notation */
    int ponder_allowed; /* allow pondering - disabled by 'easy' xboard command */

	int max_depth;		/* max search depth */
	int max_time;		/* max search time */
	int inc_time;		/* add time after move */

	int chk_time;		/* check for exit every chk_time nodes */
	int time_div;		/* assume all time for time_div moves */
	int fixed_time;		/* don't stop search before max_time elapsed */
	uint32_t stop_time; 
	uint32_t max_stop_time; 
	int thinking;		/* engine is thinking now */
	int pondering;		/* engine is pondering now */
	int ponderhit;
	pthread_t tid;		/* thread id */

	int pawn_hash_size;	/* size of pawn hash table */
	int main_hash_size;	/* size of main hash table */
	int pawn_hash_enabled;	/* use pawn hashtable */
	int main_hash_enabled;	/* use main hashtable */

	int resign;			/* if TRUE engine resigs in bad positions */
	int resign_value;   /* resign if score is worse than this value */
	int hopeless_moves;	/* moves that made after score worse than 
						   resign_value */

	int delta_pruning;
	int futility_pruning;
	int iid;
	int null_pruning;
	int lmr;
	int razoring;

	pthread_mutex_t mutex;
	pthread_mutex_t ponder_mutex;
};

extern struct engine_t e;
extern struct parsed_moves_t pm;
extern pthread_cond_t cond_exit;

/* polyglot opening book functions */
void book_close(void);
void book_open(const char file_name[]); 
int book_move(void);

void new_game(void);
void print_stats(int timediff);
void start_engine(void);
void perform_cleanup(void);

void loopcmd(void);
int check_time(int ply);
uint32_t get_time(void);
int get_engine_value(int *val);
void set_engine_value(int *src, int new_value);

/* these functions run in separate threads */
void *process_turn(void *args);
void *run_epd_test(void *args);
