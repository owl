/*
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#include "common.h"
#include "attacks.h"
#include "board.h"
#include "engine.h"
#include "move.h"

const char pieces[] = {' ', 'p', 'n', 'b', 'r', 'q', 'k'};

/* move in coordinate notation */
char *
coord_move(int move, char *buf)
{
	assert(move);

	if (GET_PROMOTE(move))
		snprintf(buf, 6, "%s%s%c", char_board[GET_FROM(move)], \
			char_board[GET_TO(move)], pieces[GET_PROMOTE(move)]);
	else
		snprintf(buf, 5, "%s%s", char_board[GET_FROM(move)], \
			char_board[GET_TO(move)]);

	return buf;
}

/* PV in coordinate notation */
char *
get_pv_string_coord(char *buf)
{
	int i;
	char move_buf[256];

	buf[0] = '\0';
	for (i = 0; i < pv_length[0]; i++) {
		strcat(buf, coord_move(pv[0][i], move_buf));
		strcat(buf, " ");
	}
	strcat(buf, "\n");

	return buf;
}

/* PV in SAN notation */
char *
get_pv_string_san(char *buf)
{
	int i;
	char move_buf[256];
	char move_buf2[256];

	if (pv_length[0]) {
		if (brd.wtm)
			sprintf(buf, "%d. ... ", (game_history.count + 1) / 2 + \
					(game_history.count ? 0 : 1));
		else
			sprintf(buf, "%d. ", (game_history.count + 1) / 2 + 1);

		for (i = 0; i < pv_length[0]; i++) {
			if (i && !brd.wtm) {
				sprintf(move_buf, "%d. ", (game_history.count + 1) / 2 + 1);
				strcat(buf, move_buf);
			}
			sprintf(move_buf, "%s ", san_move(pv[0][i], move_buf2));
			strcat(buf, move_buf);

			make_move(pv[0][i], FALSE);
		}
		strcat(buf, "\n");

		for (i = 0; i < pv_length[0]; i++)
			takeback();
	} else
		assert(0 == 1);		/* no PV! */

	return buf;
}

/* make move and update board info */
int 
make_move(int move, int tb)
{
	int ep_square;
	int to, from, type;
	int cap_piece, piece_from, piece_to;
	int go_castle;
	int xwtm;

	assert(board_is_legal(&brd));
	assert(move_is_legal(move));

	from = GET_FROM(move);	/* 'from' coordinate */
	to = GET_TO(move);		/* 'to' coordinate */
	type = GET_TYPE(move);	/* type flags */
	go_castle = FALSE;
	xwtm = 1 ^ brd.wtm;

	if (type & TYPE_CASTLE) {
		/* check if castle move is legal here */
		if (to == G1) {
			if (IS_ATTACKED(E1, BLACK) || IS_ATTACKED(F1, BLACK) || \
					IS_ATTACKED(G1, BLACK))
				return FALSE;
		} else if (to == C1) {
			if (IS_ATTACKED(E1, BLACK) || IS_ATTACKED(D1, BLACK) || \
					IS_ATTACKED(C1, BLACK))
				return FALSE;
		} else if (to == G8) {
			if (IS_ATTACKED(E8, WHITE) || IS_ATTACKED(F8, WHITE) || \
					IS_ATTACKED(G8, WHITE))
				return FALSE;
		} else if (to == C8) {
			if (IS_ATTACKED(E8, WHITE) || IS_ATTACKED(D8, WHITE) || \
					IS_ATTACKED(C8, WHITE))
				return FALSE;
		}
		go_castle = TRUE;
	}

	cap_piece = brd.cap_piece = square64[to];
	brd.move = move;

	/* copy parameters for takeback */
	memcpy(&game_history.board[game_history.count++], &brd, \
			sizeof(struct board_t));

	piece_from = square64[from];

	if (brd.ep != -1)
		brd.hash_key ^= hash_ep[_FILE(brd.ep)];		  /* clear ep hash */

	if (cap_piece)
		/* remove captured piece */
		remove_piece(to, cap_piece, xwtm);

	/* handle promote move */
	piece_to = (type & TYPE_PROMOTE) ? GET_PROMOTE(move) : piece_from;

	remove_piece(from, piece_from, brd.wtm);
	add_piece(to, piece_to, brd.wtm);
	
	if (type & TYPE_ENPASSANT) {
		/* remove enpassant victim */
		ep_square = to - (brd.wtm? -1 : 1) * 8;
		remove_piece(ep_square, PAWN, xwtm);
	}

	brd.ep = -1;	/* remove ep status by default */
	if (type & TYPE_PAWN_TWO)
		/* polyglot-compatibility hack! if there is no pawn on enpassant
		   target square skip ep */
		if ((BIT(to - 1) & PAWNS(xwtm)) || (BIT(to + 1) & PAWNS(xwtm))) {
			brd.ep = (to + from) / 2;
			brd.hash_key ^= hash_ep[_FILE(brd.ep)];
		}

	if (go_castle) {
		if (to == G1) {
			add_piece(F1, ROOK, brd.wtm);
			remove_piece(H1, ROOK, brd.wtm);
			castled[WHITE] = TRUE;
		} else if (to == C1) {
			add_piece(D1, ROOK, brd.wtm);
			remove_piece(A1, ROOK, brd.wtm);
			castled[WHITE] = TRUE;
		} else if (to == G8) {
			add_piece(F8, ROOK, brd.wtm);
			remove_piece(H8, ROOK, brd.wtm);
			castled[BLACK] = TRUE;
		} else if (to == C8) {
			add_piece(D8, ROOK, brd.wtm);
			remove_piece(A8, ROOK, brd.wtm);
			castled[BLACK] = TRUE;
		}
	}

	if (brd.castle_mask) {
		if (from == E1) {
			if (brd.castle_mask & WHITE_CASTLE_KINGSIDE)
				brd.hash_key ^= hash_wck;
			if (brd.castle_mask & WHITE_CASTLE_QUEENSIDE)
				brd.hash_key ^= hash_wcq;
			brd.castle_mask &= ~(WHITE_CASTLE_KINGSIDE | WHITE_CASTLE_QUEENSIDE);
		} else if (from == E8) {
			if (brd.castle_mask & BLACK_CASTLE_KINGSIDE)
				brd.hash_key ^= hash_bck;
			if (brd.castle_mask & BLACK_CASTLE_QUEENSIDE)
				brd.hash_key ^= hash_bcq;
			brd.castle_mask &= ~(BLACK_CASTLE_KINGSIDE | BLACK_CASTLE_QUEENSIDE);
		} else {
			if (from == H1 || to == H1) {
				if (brd.castle_mask & WHITE_CASTLE_KINGSIDE)
					brd.hash_key ^= hash_wck;
				brd.castle_mask &= ~WHITE_CASTLE_KINGSIDE;
			} 
			if (from == A1 || to == A1) {
				if (brd.castle_mask & WHITE_CASTLE_QUEENSIDE)
					brd.hash_key ^= hash_wcq;
				brd.castle_mask &= ~WHITE_CASTLE_QUEENSIDE;
			}
			if (from == H8 || to == H8) {
				if (brd.castle_mask & BLACK_CASTLE_KINGSIDE)
					brd.hash_key ^= hash_bck;
				brd.castle_mask &= ~BLACK_CASTLE_KINGSIDE;
			}
			if (from == A8 || to == A8) {
				if (brd.castle_mask & BLACK_CASTLE_QUEENSIDE)
					brd.hash_key ^= hash_bcq;
				brd.castle_mask &= ~BLACK_CASTLE_QUEENSIDE;
			}
		}
	}
	/* if the move is not a capture or pawn push, increase fifty_rule */
	brd.fifty_rule = (!type || type == TYPE_CASTLE)? brd.fifty_rule + 1 : 0;

	brd.wtm = xwtm;
	brd.hash_key ^= hash_side;

	if (IS_CHECKED(1 ^ brd.wtm)) {
		takeback();
		return FALSE;
	} else {
		if (tb) 
			takeback();
		counters.searched_nodes++;
		return TRUE;
	}
}

/* used for sanity check */
int 
move_is_legal(int move)
{
	int from = GET_FROM(move);
	int to = GET_TO(move);

	if (to == from)
		return FALSE;

	int piece_from = square64[from];
	int piece_to = square64[to];

	if (!piece_from)
		return FALSE;

	if ((BIT(from) & piece_boards[brd.wtm][piece_from]) == 0)
		return FALSE;

	if (piece_to) {
		if (BIT(to) & side_boards[brd.wtm])
			return FALSE;
		if (piece_to == KING)
			return FALSE;
	}

	return TRUE;
}

/* return internal binary move representation from coordinate */
int 
parse_move_coord(char *move)
{
	char buf[256];
	int i;
	struct moves_t moves;

	gen_legal_moves(&moves);

	/* generate pseudolegal moves and find target move */
	for (i = 0; i < moves.count; i++) {
		if (!strcmp(move, coord_move(moves.move[i], buf)))
			return moves.move[i];
	}
	return 0;
}

/* return internal binary move representation from SAN */
int 
parse_move_san(char *move)
{
	int i;
	char buf[MAX_STRING];
	struct moves_t moves;

	gen_legal_moves(&moves);

	/* generate pseudolegal moves and find target move */
	for (i = 0; i < moves.count; i++) {
		if (!strncmp(move, san_move(moves.move[i], buf), 6))
			return moves.move[i];
	}

	return 0;
}

void 
print_move_coord(int move)
{
	char buf[256];
	printf("%s\n", coord_move(move, buf));
}

void 
print_moves_coord(struct moves_t *moves)
{
	int i;
	char buf[256];

	for (i = 0; i < moves->count; i++)
		printf("%s ", coord_move(moves->move[i], buf));
	printf("\n");
}

void 
print_moves_san(struct moves_t *moves)
{
	int i;
	char buf[256];

	for (i = 0; i < moves->count; i++) {
		if (make_move(moves->move[i], TRUE))
			printf("%s ", san_move(moves->move[i], buf));
	}
	printf("\n");
}

/* undo move */
void 
takeback(void)
{
	int c;
	int cap_piece;
	int move;
	int from, to, type;
	int to_piece;
	int xwtm;

	assert(board_is_legal(&brd));

	c = --game_history.count;
	cap_piece = game_history.board[c].cap_piece;
	move = game_history.board[c].move;
	
	brd.castle_mask = game_history.board[c].castle_mask;
	brd.ep = game_history.board[c].ep;

	/* do the following if move is not 'NULL move' */
	if (move) {
		from = GET_FROM(move);
		to = GET_TO(move);
		type = GET_TYPE(move);
		to_piece = square64[to];
		xwtm = 1 ^ brd.wtm;

		remove_piece(to, to_piece, xwtm);

		if (cap_piece) {
			/* restore captured piece */
			add_piece(to, cap_piece, brd.wtm);
			add_piece(from, (type & TYPE_PROMOTE)? PAWN : to_piece, xwtm);
		} else if (type & TYPE_ENPASSANT) {
			/* restore captured by enpassant pawn */
			add_piece(brd.wtm? to - 8 : to + 8, PAWN, brd.wtm);
			add_piece(from, PAWN, xwtm);
		} else if (type & TYPE_PROMOTE)
			add_piece(from, PAWN, xwtm);
		else
			add_piece(from, to_piece, xwtm);

		/* undo castle */
		if (type & TYPE_CASTLE) {
			if (to == G1) {
				add_piece(H1, ROOK, xwtm);
				remove_piece(F1, ROOK, xwtm);
				castled[WHITE] = FALSE;
			} else if (to == C1) {
				add_piece(A1, ROOK, xwtm);
				remove_piece(D1, ROOK, xwtm);
				castled[WHITE] = FALSE;
			} else if (to == G8) {
				add_piece(H8, ROOK, xwtm);
				remove_piece(F8, ROOK, xwtm);
				castled[BLACK] = FALSE;
			} else if (to == C8) {
				add_piece(A8, ROOK, xwtm);
				remove_piece(D8, ROOK, xwtm);
				castled[BLACK] = FALSE;
			}
		}
	}
	brd.wtm = 1 ^ brd.wtm;
	brd.hash_key = game_history.board[c].hash_key;
	brd.hash_pawn_key = game_history.board[c].hash_pawn_key;
	brd.fifty_rule = game_history.board[c].fifty_rule;
}
