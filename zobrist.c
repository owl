/*
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#include "common.h"
#include "board.h"

uint64_t hash_wck;
uint64_t hash_wcq;
uint64_t hash_bck;
uint64_t hash_bcq;
uint64_t hash_code[2][7][64];
uint64_t hash_ep[8];
uint64_t hash_side;

void 
initialize_zobrist()
{
	int p;
	int color, piece, square;

	for (color = WHITE; color <= BLACK; color++)
		for (piece = PAWN; piece <= KING; piece++)
			for (square = 0; square < 64; square++) {
				p = piece * 2 - color - 1;
				hash_code[color][piece][square] = \
					  p_hash_piece[64 * p + _RANK(square) * 8 + _FILE(square)];
			}

	for (square = 0; square < 8; square++)
		hash_ep[square] = p_hash_ep[square];

	hash_wck = p_hash_castle[0];
	hash_wcq = p_hash_castle[1];
	hash_bck = p_hash_castle[2];
	hash_bcq = p_hash_castle[3];
	hash_side = p_hash_side[0];
}

void 
calc_zobrist(uint64_t *hash_key, uint64_t *hash_pawn_key)
{
	int sq, piece, color; 
	uint64_t b;

	*hash_key = *hash_pawn_key = 0ULL;

	for (color = WHITE; color <= BLACK; color++) {
		for (piece = PAWN; piece <= KING; piece++) {
			b = piece_boards[color][piece];
			while (b) {
				sq = bit_scan(b);
				CLEAR (b, sq); 
				*hash_key ^= hash_code[color][piece][sq];
				if (piece == PAWN)
					*hash_pawn_key ^= hash_code[color][piece][sq];
			}
		}
	}
	if (brd.castle_mask & WHITE_CASTLE_KINGSIDE)
		*hash_key ^= hash_wck;
	if (brd.castle_mask & WHITE_CASTLE_QUEENSIDE)
		*hash_key ^= hash_wcq;
	if (brd.castle_mask & BLACK_CASTLE_KINGSIDE)
		*hash_key ^= hash_bck;
	if (brd.castle_mask & BLACK_CASTLE_QUEENSIDE)
		*hash_key ^= hash_bcq;

	if (brd.ep != -1)
		*hash_key ^= hash_ep[_FILE(brd.ep)];
	if (brd.wtm == WHITE)
		*hash_key ^= hash_side;
}
