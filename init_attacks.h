/*
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
void initialize_bishop_attacks();
void initialize_rook_attacks();
void initialize_attacks();
uint64_t index_to_bitboard(int index, uint64_t mask);
uint64_t sliding_attacks(int square, uint64_t block, int dirs, \
		int deltas[][2], int fmin, int fmax, int rmin, int rmax); 
