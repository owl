/*
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#include <assert.h>
#include <errno.h>
#include <inttypes.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

enum COLOR {NOBODY = -1, WHITE = 0, BLACK = 1};
enum PIECES {NONE, PAWN, KNIGHT, BISHOP, ROOK, QUEEN, KING};
enum SQUARES64 {
	A1, B1, C1, D1, E1, F1, G1, H1, 
	A2, B2, C2, D2, E2, F2, G2, H2, 
	A3, B3, C3, D3, E3, F3, G3, H3, 
	A4, B4, C4, D4, E4, F4, G4, H4, 
	A5, B5, C5, D5, E5, F5, G5, H5, 
	A6, B6, C6, D6, E6, F6, G6, H6, 
	A7, B7, C7, D7, E7, F7, G7, H7, 
	A8, B8, C8, D8, E8, F8, G8, H8 };

#define TRUE 1
#define FALSE 0

#define MAX_STRING 2048

#define MAX_PLY 64

#define OPENING 0
#define ENDGAME 1

#define MAX(a, b)		   ((a) > (b) ? (a) : (b))
#define MIN(a, b)		   ((a) < (b) ? (a) : (b))

#define _FILE(x)			((x) & 7)
#define _RANK(x)			((x) >> 3)

#define ZERO_MEM(x)			(memset (&(x), 0, sizeof (x)))

#define MATE_VALUE 30000
#define SCORE_IS_MATE(x)	((x) > MATE_VALUE - 512 || (x) < -MATE_VALUE + 512)

/* simple bit operations */
#define BIT(x)			   (1ULL << (x))
#define SET(bb, i)		   ((bb) |= BIT(i))
#define CLEAR(bb, i)	   ((bb) &= ~BIT(i))

#define _RANK1 0x00000000000000FFULL
#define _RANK2 0x000000000000FF00ULL
#define _RANK7 0x00FF000000000000ULL
#define _RANK8 0xFF00000000000000ULL

#define WHITESQUARES  0x55AA55AA55AA55AAULL
#define BLACKSQUARES  0xAA55AA55AA55AA55ULL

#define GAME_PHASE MAX((256 - (int)(256.0 * (wbc + wnc + wrc * 2 + wqc * 8 + \
	bnc + bbc + brc * 2 + bqc * 8) / 32.0)), 0)

/* global data */
extern char program_name[MAX_STRING];

/* zobrist hash */
extern uint64_t hash_wck;
extern uint64_t hash_wcq;
extern uint64_t hash_bck;
extern uint64_t hash_bcq;
extern uint64_t hash_code[2][7][64];
extern uint64_t hash_ep[8];
extern uint64_t hash_side;

extern uint64_t *p_hash_piece;
extern uint64_t *p_hash_castle;
extern uint64_t *p_hash_ep;
extern uint64_t *p_hash_side;

/* Bitwise operations */
extern int bitcount[65536];

int bit_scan(uint64_t b);
int bit_scan_clear(uint64_t *b);
int bit_scan_rev(uint64_t b);
static inline int popcount(uint64_t b)
{
	return (bitcount[(b) >> 48] + bitcount[((b) >> 32) & 0xffff] + \
			bitcount[((b) >> 16) & 0xffff] + bitcount[(b) & 0xffff]);
}

