/*
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#include "common.h"
#include "init.h"
#include "engine.h"
#include "trans.h"

/* engine name */
char program_name[MAX_STRING];

int main(int argc, char **argv)
{
	int opt;

#ifdef STATISTIC_COUNTERS
	snprintf(program_name, MAX_STRING, "owl %s, build %s", PACKAGE_VERSION, __DATE__);
#else
	snprintf(program_name, MAX_STRING, "owl %s", PACKAGE_VERSION); 
#endif
	/* turn off I/O buffering */
	setbuf(stdin, 0);
	setbuf(stdout, 0);
	
	initialize_bitboards();
	initialize_zobrist();

	start_engine();

	/* parse command line parameters */
	while ((opt = getopt(argc, argv, "h:p:")) != -1)
		switch (opt) {
			case 'h':
				if ((e.main_hash_size = atoi(optarg)) <= 0)
					e.main_hash_enabled = FALSE;
				break;
			case 'p':
				if ((e.pawn_hash_size = atoi(optarg)) <= 0)
					e.pawn_hash_enabled = FALSE;
				break;
		}

	/* allocate memory for hashtables */
	if (initialize_pawn_hashtable()) {
		e.pawn_hash_enabled = FALSE;
		fprintf(stderr, "Can't allocate pawn hashtable!\n");
	}
	if (initialize_main_hashtable()) {
		e.main_hash_enabled = FALSE;
		fprintf(stderr, "Can't allocate main hashtable!\n");
	}

	/* clear transposition tables */
	clear_pawn_hashtable();
	clear_main_hashtable();

	loopcmd();

	return 0;
}
