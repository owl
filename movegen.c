/*
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#include <assert.h>
#include "common.h"
#include "attacks.h"
#include "board.h"
#include "engine.h"
#include "move.h"

#define CreateMove(from, to, type, piece) (SET_FROM(from) | SET_TO(to) | SET_TYPE(type) | SET_PROMOTE(piece))

const int sign_8[2] = {-8, 8};
const int sign_16[2] = {-16, 16};
const int ks[2] = {WHITE_CASTLE_KINGSIDE, BLACK_CASTLE_KINGSIDE};
const int qs[2] = {WHITE_CASTLE_QUEENSIDE, BLACK_CASTLE_QUEENSIDE};

/* forward declarations of functions */
static int cap(int move, int side, int *see);
static int gen_non_quiescent_moves(struct moves_t *moves);

/* pawn pushes */
static void gen_pawn_push_one(int side, struct moves_t *moves);
static void gen_pawn_push_two(int side, struct moves_t *moves);

/* piece moves including captures */
static void gen_knight(int side, int capture, uint64_t mask, struct moves_t *moves);
static void gen_bishop(int side, int capture, uint64_t mask, struct moves_t *moves);
static void gen_rook(int side, int capture, uint64_t mask, struct moves_t *moves);
static void gen_queen(int side, int capture, uint64_t mask, struct moves_t *moves);
static void gen_king(int side, int capture, uint64_t mask, struct moves_t *moves);

/* castle moves */
static void gen_castle(int side, struct moves_t *moves);

/* pawn promotes */
static void gen_pawn_promote_minor(int side, struct moves_t *moves);
static void gen_pawn_promote_queen(int side, struct moves_t *moves);

/* pawn promotes and captures */
static void gen_pawn_cap_promote(int side, struct moves_t *moves);

/* score for move ordering */
static int 
cap(int move, int side, int *see)
{
	*see = SEE(move);
	if (*see < 0)
		return (*see);
	else
		return (*see) + 10000000;
}

/* return only legal moves */
int 
gen_legal_moves(struct moves_t *moves)
{
	int i;
	struct moves_t tmp_moves;

	moves->count = 0;
	gen_moves(&tmp_moves);

	for (i = 0; i < tmp_moves.count; i++) {
		if (make_move(tmp_moves.move[i], TRUE)) {
			moves->move[moves->count] = tmp_moves.move[i];
			moves->score[moves->count] = tmp_moves.score[i];
			moves->see[moves->count] = tmp_moves.see[i];
			moves->count++;
		}
	}
	return moves->count;
}

/* generate all pseudo legal moves */
int 
gen_moves(struct moves_t *moves)
{
	gen_quiescent_moves(moves, side_boards[1 ^ brd.wtm]);
	gen_non_quiescent_moves(moves);

	return moves->count;
}

/* generate captures and queen promotions */
int 
gen_quiescent_moves(struct moves_t *moves, uint64_t target)
{
	moves->count = 0;

	gen_pawn_cap_promote(brd.wtm, moves);
	gen_knight(brd.wtm, TRUE, target, moves);
	gen_bishop(brd.wtm, TRUE, target, moves);
	gen_rook(brd.wtm, TRUE, target, moves);
	gen_queen(brd.wtm, TRUE, target, moves);
	gen_king(brd.wtm, TRUE, target, moves);
	gen_pawn_promote_queen(brd.wtm, moves);

	return moves->count;
}

/* generate non-captures and non-queen promotions */
static int 
gen_non_quiescent_moves(struct moves_t *moves)
{
	uint64_t target = ~complete;

	gen_pawn_push_one(brd.wtm, moves);
	gen_pawn_push_two(brd.wtm, moves);
	gen_knight(brd.wtm, FALSE, target, moves);
	gen_bishop(brd.wtm, FALSE, target, moves);
	gen_rook(brd.wtm, FALSE, target, moves);
	gen_queen(brd.wtm, FALSE, target, moves);
	gen_pawn_promote_minor(brd.wtm, moves);
	gen_king(brd.wtm, FALSE, target, moves);
	gen_castle(brd.wtm, moves);

	return moves->count;
}

static void 
gen_pawn_push_one(int side, struct moves_t *moves)
{
	int to;
	uint64_t move_mask;

	/* remove pawns that can be promoted (there is another routine 
	   for them) */
	move_mask = side ? ((PAWNS(BLACK) >> 8) & ~complete & ~_RANK1) : \
		((PAWNS(WHITE) << 8) & ~complete & ~_RANK8);

	while (move_mask) {
		to = bit_scan_clear(&move_mask);
		moves->move[moves->count] = CreateMove(to + sign_8[side], to, \
				TYPE_PAWN_ONE, 0);
		moves->score[moves->count] = history[side][PAWN][(to + sign_8[side]) | \
				(to << 6)];
		moves->count++;
	}
}

static void 
gen_pawn_push_two(int side, struct moves_t *moves)
{
	int to;
	uint64_t move_mask;

	/* mask pawns standing on their initial positions */
	move_mask = side ? (((PAWNS(BLACK) & _RANK7) >> 16) & \
		(~complete & (~complete >> 8))) : (((PAWNS(WHITE) & _RANK2) << 16) & \
		(~complete & (~complete << 8)));

	while (move_mask) {
		to = bit_scan_clear(&move_mask);
		moves->move[moves->count] = CreateMove(to + sign_16[side], to, \
				TYPE_PAWN_TWO, 0);
		moves->score[moves->count] = history[side][PAWN][(to + sign_16[side]) | \
				(to << 6)];
		moves->count++;
	}
}

static void 
gen_knight(int side, int capture, uint64_t mask, struct moves_t *moves)
{
	int from, to;
	uint64_t move_mask;
	uint64_t pieces; 
	
	pieces = KNIGHTS(side);

	while (pieces) {
		from = bit_scan_clear(&pieces);
		move_mask = piece_moves[KNIGHT][from] & mask;

		while (move_mask) {
			to = bit_scan_clear(&move_mask);
			moves->move[moves->count] = CreateMove(from, to, \
					capture ? TYPE_CAPTURE : 0, 0);
			moves->score[moves->count] = capture ? \
				cap(moves->move[moves->count], side, \
				&moves->see[moves->count]) : history[side][KNIGHT][from | \
				(to << 6)];
			moves->count++;
		}
	}
}

static void 
gen_bishop(int side, int capture, uint64_t mask, struct moves_t *moves)
{
	int from, to;
	uint64_t move_mask;
	uint64_t pieces; 
	
	pieces = BISHOPS(side);

	while (pieces) {
		from = bit_scan_clear(&pieces);
		move_mask = BISHOP_ATTACKS(from, complete) & mask;

		while (move_mask) {
			to = bit_scan_clear(&move_mask);
			moves->move[moves->count] = CreateMove(from, to, \
					capture ? TYPE_CAPTURE : 0, 0);
			moves->score[moves->count] = capture ? \
				cap(moves->move[moves->count], side, \
				&moves->see[moves->count]) : history[side][BISHOP][from | \
				(to << 6)];
			moves->count++;
		}
	}
}

static void 
gen_rook(int side, int capture, uint64_t mask, struct moves_t *moves)
{
	int from, to;
	uint64_t move_mask;
	uint64_t pieces; 
		
	pieces = ROOKS(side);

	while (pieces) {
		from = bit_scan_clear(&pieces);
		move_mask = ROOK_ATTACKS(from, complete) & mask;

		while (move_mask) {
			to = bit_scan_clear(&move_mask);
			moves->move[moves->count] = CreateMove(from, to, \
					capture ? TYPE_CAPTURE : 0, 0);
			moves->score[moves->count] = capture ? \
				cap(moves->move[moves->count], side, \
				&moves->see[moves->count]) : history[side][ROOK][from | \
				(to << 6)];
			moves->count++;
		}
	}
}

static void 
gen_queen(int side, int capture, uint64_t mask, struct moves_t *moves)
{
	int from, to;
	uint64_t move_mask;
	uint64_t pieces;
		
	pieces = QUEENS(side);

	while (pieces) {
		from = bit_scan_clear(&pieces);
		move_mask = QUEEN_ATTACKS(from, complete) & mask;

		while (move_mask) {
			to = bit_scan_clear(&move_mask);
			moves->move[moves->count] = CreateMove(from, to, \
					capture ? TYPE_CAPTURE : 0, 0);
			moves->score[moves->count] = capture ? \
				cap(moves->move[moves->count], side, \
				&moves->see[moves->count]) : history[side][QUEEN][from | \
				(to << 6)];
			moves->count++;
		}
	}
}

static void 
gen_king(int side, int capture, uint64_t mask, struct moves_t *moves)
{
	int from, to;
	uint64_t move_mask;

	from = brd.kings[side];
	move_mask = piece_moves[KING][from] & mask;

	while (move_mask) {
		to = bit_scan_clear(&move_mask);
		moves->move[moves->count] = CreateMove(from, to, \
				capture ? TYPE_CAPTURE : 0, 0);
		moves->score[moves->count] = capture ? \
			cap(moves->move[moves->count], side, \
			&moves->see[moves->count]) : history[side][KING][from | \
			(to << 6)];
		moves->count++;
	}
}

static void 
gen_castle(int side, struct moves_t *moves)
{
	if (!brd.castle_mask)
		return;
	
	if ((brd.castle_mask & ks[side]) && !square64[F1 + side * (F8 - F1)] && \
			!square64[G1 + side * (G8 - G1)]) {
		moves->move[moves->count] = CreateMove(E1 + side * (E8 - E1), \
				G1 + side * (G8 - G1), TYPE_CASTLE, 0);
		moves->score[moves->count] = history[side][KING][(E1 + side * (E8 - E1))\
					 | ((G1 + side * (G8 - G1)) << 6)];
		moves->count++;
	}
	if ((brd.castle_mask & qs[side]) && !square64[B1 + side * (B8 - B1)] && \
			!square64[C1 + side * (C8 - C1)] && !square64[D1 + side * (D8 - D1)]) {
		moves->move[moves->count] = CreateMove(E1 + side * (E8 - E1), \
				C1 + side * (C8 - C1), TYPE_CASTLE, 0);
		moves->score[moves->count] = history[side][KING][(E1 + side * (E8 - E1))\
					 | ((C1 + side * (C8 - C1)) << 6)];
		moves->count++;
	}
}

static void 
gen_pawn_promote_minor(int side, struct moves_t *moves)
{
	int piece, to;
	uint64_t mask;
   
	mask = side == WHITE? (PAWNS(WHITE) << 8) & ~complete & _RANK8 : \
					(PAWNS(BLACK) >> 8) & ~complete & _RANK1;
	while (mask) {
		to = bit_scan_clear(&mask);

		for (piece = KNIGHT; piece < QUEEN; piece++) {
			moves->move[moves->count] = CreateMove(to + sign_8[side], to, \
					TYPE_PROMOTE, piece);
			moves->score[moves->count] = cap(moves->move[moves->count], side, &moves->see[moves->count]);
			if (moves->score[moves->count] < 0)
				moves->score[moves->count] = -piece_value[PAWN];
			moves->count++;
		}
	}
}

static void 
gen_pawn_promote_queen(int side, struct moves_t *moves)
{
	int to;
	uint64_t mask;
   
	mask = side == WHITE? (PAWNS(WHITE) << 8) & ~complete & _RANK8 :\
					(PAWNS(BLACK) >> 8) & ~complete & _RANK1;
	while (mask) {
		to = bit_scan_clear(&mask);

		moves->move[moves->count] = CreateMove(to + sign_8[side], to, \
				TYPE_PROMOTE, QUEEN);
		moves->score[moves->count] = cap(moves->move[moves->count], side, &moves->see[moves->count]);
		if (moves->score[moves->count] < 0)
			/* small bonus for queen promotions */
			moves->score[moves->count] = -piece_value[PAWN] + 1;
		moves->count++;
	}
}

static void 
gen_pawn_cap_promote(int side, struct moves_t *moves)
{
	int k, l;
	int piece;
	int to;
	uint64_t mask;
	uint64_t enemies;
   
	enemies = side_boards[1 ^ side] | (brd.ep == -1? 0 : BIT(brd.ep));

	if (side == WHITE) {
		for (k = 0, l = 0; k <= 2; k += 2, l += 7) {
			mask = ((PAWNS(WHITE) & ~file_bit[l]) << (7 + k)) & enemies;
			while (mask) {
				to = bit_scan_clear(&mask);

				if (to >= 56) {
					for (piece = KNIGHT; piece < KING; piece++) {
						moves->move[moves->count] = CreateMove(to - 7 - k, \
							to, TYPE_PROMOTE | TYPE_CAPTURE, piece);
						moves->score[moves->count] = cap(moves->move[moves->count], side, \
								&moves->see[moves->count]);
						moves->count++;
					}
				} else {
					moves->move[moves->count] = CreateMove(to - 7 - k, to, \
						(to != brd.ep? TYPE_CAPTURE : TYPE_ENPASSANT), 0);
					moves->score[moves->count] = \
						cap(moves->move[moves->count], \
						side, &moves->see[moves->count]);
					moves->count++;
				}
			}
		}
	} else {
		for (k = 0, l = 7; k <= 2; k += 2, l -= 7) {
			mask = ((PAWNS(BLACK) & ~file_bit[l]) >> (7 + k)) & enemies;
			while (mask) {
				to = bit_scan_clear(&mask);

				if (to <= 7) {
					for (piece = KNIGHT; piece < KING; piece++) {
						moves->move[moves->count] = CreateMove(to + 7 + k, \
							to, TYPE_PROMOTE | TYPE_CAPTURE, piece);
						moves->score[moves->count] = cap(moves->move[moves->count], side, \
								&moves->see[moves->count]);
						moves->count++;
					}
				} else {
					moves->move[moves->count] = CreateMove(to + 7 + k, to, \
						(to != brd.ep? TYPE_CAPTURE : TYPE_ENPASSANT), 0);
					moves->score[moves->count] = \
						cap(moves->move[moves->count], \
						side, &moves->see[moves->count]);
					moves->count++;
				}
			}
		}
	}
}
