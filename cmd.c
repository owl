/*
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#include "common.h"
#include "board.h"
#include "engine.h"
#include "evaluate.h"
#include "move.h"
#include "search.h"

/* forward declarations of functions */
static void init_protover_2(void);
static int parse_cmd(char *cmd);

/* wait for a command */
void 
loopcmd(void)
{
	int i;
	int res;
	int move;
	int engine_xboard;
	char cmd[MAX_STRING];
	char move_buf[MAX_STRING];
	pthread_t tid;

	while (TRUE) {
		if (get_engine_value(&e.side) == brd.wtm &&
				!get_engine_value(&e.force_mode)) {
			/* start thinking in separate thread */
			res = pthread_create(&tid, 0, process_turn, 0);

			/* error occured */
			if (res) {
				fprintf(stderr, "pthread error: %s\n", strerror(res));
				perform_cleanup();
				exit(res);
			}

			pthread_mutex_lock(&e.mutex);
			e.tid = tid;
			e.side = NOBODY;
			pthread_mutex_unlock(&e.mutex);
				
		} else {
			engine_xboard = get_engine_value(&e.xboard_mode);
			if (!engine_xboard && !get_engine_value(&e.thinking))
				fprintf(stdout, "[%s]$ ", program_name);

			/* Ctrl+D */
			if (!fgets(cmd, MAX_STRING, stdin)) {
				perform_cleanup();
				exit(0);
			}

			if (!strcmp(cmd, "\n"))
				continue;

			if (parse_cmd(cmd))
				continue;

			/* check if we have received a move */
			sscanf(cmd, "%s", move_buf);

			/* parse according to the notation we are using */
			if (get_engine_value(&e.san_notation) || !engine_xboard) {
				if (!get_engine_value(&e.pondering))
					move = parse_move_san(move_buf);
				else {
					move = 0;
					for (i = 0; i < pm.count; i++)
						if (strcmp(pm.sans[i], move_buf) == 0) {
							move = pm.move[i];
							break;
						}
				}
			} else
				move = parse_move_coord(move_buf);

			if (!move)
				fprintf(stdout, "Error (unknown command): %s\n", move_buf);
			else {
				/* OK, it is a legal move - make it */

				if (return_ponder[0] && !strcmp(return_ponder, move_buf)) {
					printf("[ Ponder Hit ]\n");
					set_engine_value((int *)&e.pondering, 0);
					set_engine_value((int *)&e.ponderhit, TRUE);
					pthread_cond_signal(&cond_exit);
				} else {
					if (return_ponder[0]) {
						printf("[ Ponder Fail :( ]\n");

						/* stop pondering */
						set_engine_value((int *)&e.pondering, 0);
						set_engine_value((int *)&e.ponderhit, FALSE);
						set_engine_value((int *)&e.stop_time, 0);
						set_engine_value((int *)&e.max_stop_time, 0);
						pthread_cond_signal(&cond_exit);
						pthread_join(tid, NULL);
						takeback();
					}

					make_move(move, FALSE);
					set_engine_value(&e.side, brd.wtm);
				}
			}
		}
	}
}

static int 
parse_cmd(char *cmd)
{
	int val;
	int inc;
	char total_time[MAX_STRING];
	char *filename;
	pthread_t tid;
	struct epd_info_t *ei;
	struct moves_t moves;

	/* start thinking */
	if (!strncmp(cmd, "go", 2)) {
		if (!get_engine_value(&e.thinking)) {
			set_engine_value(&e.force_mode, FALSE);
			set_engine_value(&e.side, brd.wtm);
		}
		/* display current board to console */
	} else if (!strncmp(cmd, "board", 5))
		print_board();

	/* xboard send this command */
	else if (!strncmp(cmd, "xboard", 6))
		set_engine_value(&e.xboard_mode, TRUE);
	/* stop thinking */
	else if (!strncmp(cmd, "force", 5)) {
		set_engine_value(&e.force_mode, TRUE);
		set_engine_value(&e.side, NOBODY);
		set_engine_value((int *)&e.stop_time, 0);
		set_engine_value((int *)&e.max_stop_time, 0);
		if (e.tid) {
			val = pthread_join(e.tid, 0);
			if (val)
				fprintf(stderr, "Error occured: %s\n", strerror(val));
			e.tid = 0;
		}
		/* move now command */
	} else if (!strncmp(cmd, "?", 1)) {
		set_engine_value((int *)&e.stop_time, 0); 
		set_engine_value((int *)&e.max_stop_time, 0); 
	/* the opponent is a computer */
	} else if (!strncmp(cmd, "computer", 8))
		set_engine_value(&e.play_computer, TRUE);

	/* xboard support version protocol version 2. we  should reply
	   by 'feature' command */
	else if (!strncmp(cmd, "protover 2", 10)) {
		set_engine_value(&e.san_notation, TRUE);
		init_protover_2();

		/* start new game */
	} else if (!strncmp(cmd, "new", 3)) {
		new_game();
		set_engine_value(&e.play_computer, FALSE);
		set_engine_value(&e.force_mode, FALSE);
		set_engine_value(&e.max_depth, MAX_PLY);
		set_engine_value(&e.side, BLACK);
		/* display thinking */
	} else if (!strncmp(cmd, "post", 4))
		set_engine_value(&e.post_mode, TRUE);
    else if (!strncmp(cmd, "easy", 4))
		set_engine_value(&e.ponder_allowed, FALSE);
    else if (!strncmp(cmd, "hard", 4))
		set_engine_value(&e.ponder_allowed, TRUE);
	/* do not display thinking */
	else if (!strncmp(cmd, "nopost", 6))
		set_engine_value(&e.post_mode, FALSE);

	/* time remaining for the engine */
	else if (sscanf(cmd, "time %d", &val) == 1) {
		set_engine_value(&e.max_time, val / \
				get_engine_value(&e.time_div));
		set_engine_value(&e.max_depth, MAX_PLY);
		set_engine_value(&e.fixed_time, FALSE);
		pthread_cond_signal(&cond_exit);

	} else if (sscanf(cmd, "otim %d", &val) == 1) {
		/* handle but skip this */
	} else if (sscanf(cmd, "st %d", &val) == 1) {
		set_engine_value(&e.max_time, val);
		set_engine_value(&e.max_depth, MAX_PLY);
		set_engine_value(&e.fixed_time, TRUE);

		/* set search depth */
	} else if (sscanf(cmd, "sd %d", &val) == 1) {
		set_engine_value(&e.max_time, 100000);
		set_engine_value(&e.max_depth, val);
	} else if (sscanf(cmd, "level %d %s %d", &val, &total_time, &inc) == 3) {
		set_engine_value(&e.inc_time, inc * 100);
	/* we can't change some parameters while searching */
	/* enable/disable NULL move pruning (CLI command) */
	} else if (!get_engine_value(&e.thinking) && !strncmp(cmd, "null on", 7))
		e.null_pruning = TRUE;
	else if (!get_engine_value(&e.thinking) && !strncmp(cmd, "null off", 8))
		e.null_pruning = FALSE;

	/* enable/disable Late Move Reductions (CLI command) */
	else if (!get_engine_value(&e.thinking) && !strncmp(cmd, "lmr on", 6))
		e.lmr = TRUE;
	else if (!get_engine_value(&e.thinking) && !strncmp(cmd, "lmr off", 7))
		e.lmr = FALSE;

	/* enable/disable Delta Pruning (CLI command) */
	else if (!get_engine_value(&e.thinking) && !strncmp(cmd, "delta on", 8))
		e.delta_pruning = TRUE;
	else if (!get_engine_value(&e.thinking) && !strncmp(cmd, "delta off", 9))
		e.delta_pruning = FALSE;

	/* enable/disable Futility Pruning (CLI command) */
	else if (!get_engine_value(&e.thinking) && !strncmp(cmd, "futil on", 8))
		e.futility_pruning = TRUE;
	else if (!get_engine_value(&e.thinking) && !strncmp(cmd, "futil off", 9))
		e.futility_pruning = FALSE;

	else if (!strncmp(cmd, "draw", 4)) {
	} else if (!strncmp(cmd, "epd", 3)) {
		if (!get_engine_value(&e.thinking)) {
			/* EPD - solve test positions */
			ei = malloc(sizeof(struct epd_info_t));
			filename = malloc(MAX_STRING);

			sscanf(cmd, "epd %s %d %d %d", filename, &ei->think_time, \
					&ei->max_tests, &ei->first_test);

			ei->filename = filename;
			pthread_create(&tid, 0, run_epd_test, ei);
		}
		/* setup board from FEN position */
	} else if (!strncmp(cmd, "setboard", 8)) {
		if (!setup_board(cmd + 9))
			fprintf(stdout, "Invalid FEN position: %s\n", cmd + 9);
		set_engine_value(&e.side, NOBODY);

		/* print FEN to stdout */
	} else if (!strncmp(cmd, "fen", 3))
		fprintf(stdout, "%s\n", current_fen_position(cmd));

	/* takeback last move */
	else if (!strncmp(cmd, "undo", 4)) {
		set_engine_value(&e.side, NOBODY);
		if (game_history.count)
			takeback();

		/* exit engine */
	} else if (!strncmp(cmd, "quit", 4)) {
		perform_cleanup();
		exit(0);

		/* reply with 'pong' command */
	} else if (sscanf(cmd, "ping %d", &val) == 1)
		fprintf(stdout, "pong %d\n", val);

	/* print possible moves to stdout */
	else if (!strncmp(cmd, "moves", 5)) {
		gen_legal_moves(&moves);
		print_moves_san(&moves);
	} else if (!strncmp(cmd, "eval", 4))
		printf("eval = %d\n", evaluate(-MATE_VALUE, MATE_VALUE));
	else
		/* invalid command, maybe it is a move */
		return FALSE;

	return TRUE;
}

/* default xboard parameters */
static void 
init_protover_2(void)
{
	/* disable Unix signals SIGINT and SIGTERM and tell some of our 
	   parameters */
	fprintf(stdout, "feature myname \"%s\"\n" \
		"feature san=1\n" \
		"feature colors=0\n" \
		"feature setboard=1\n" \
		"feature sigint=0\n" \
		"feature sigterm=0\n" \
		"feature ping=1\n" \
		"feature done=1\n", program_name);
}
