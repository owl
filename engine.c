/*
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#include "common.h"
#include "attacks.h"
#include "board.h"
#include "engine.h"
#include "evaluate.h"
#include "move.h"
#include "search.h"
#include "trans.h"

struct engine_t e;					/* engine parameters */

int history[2][7][4096];			/* history for move ordering */
int hash_moves[MAX_PLY];				/* moves to try first */
struct killers_t killers[MAX_PLY];	/* moves to try after hash moves */

int pv[MAX_PLY][MAX_PLY];				/* triangular array for PV */
int pv_length[MAX_PLY];

char return_ponder[32];

/* initial chess position */
char start_position[] = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq -";
char book_file_name[] = "book.bin";

/* statistic counters */
struct counters_t counters;

struct parsed_moves_t pm;

pthread_cond_t cond_exit = PTHREAD_COND_INITIALIZER;

/*
 * Return variable's value in thread-safe way
 */
int 
get_engine_value(int *val)
{
	int result;

	pthread_mutex_lock(&e.mutex);
	result = *val;
	pthread_mutex_unlock(&e.mutex);

	return result;
}

/*
 * Set value in thread-safe way
 */
void 
set_engine_value(int *src, int new_value)
{
	pthread_mutex_lock(&e.mutex);
	*src = new_value;
	pthread_mutex_unlock(&e.mutex);
}

/* time from Epoc in second/100 units - xboard time units */
uint32_t 
get_time(void)
{	
	struct timeval t;
	gettimeofday(&t, 0);

	return t.tv_sec * 100 + t.tv_usec / 10000;
} 

/* check if we should abort search */
int 
check_time(int ply)
{
	int i;

	if (!(counters.searched_nodes & e.chk_time)) {
		int stop_time = get_engine_value((int *)&e.stop_time);
		if (get_time() >= stop_time) {
			if (get_engine_value((int *)&e.pondering))
				return FALSE;

			if (fail_high_root && (stop_time < get_engine_value((int *)&e.max_stop_time) &&
							!get_engine_value(&e.fixed_time))) {
				set_engine_value((int *)&e.stop_time, stop_time + e.max_time);
				fprintf(stderr, "Extending because fail high at ply 1\n");
				return FALSE;
			}
			if (!safe_search && !get_engine_value(&e.fixed_time) &&
					stop_time < get_engine_value((int *)&e.max_stop_time)) {
				set_engine_value((int *)&e.stop_time, stop_time + e.max_time / 2);
				fprintf(stderr, "Extending...\n");
				return FALSE;
			}

			/* takeback all moves in search tree */
			for (i = 0; i < ply; i++)
				takeback();
			abort_search = TRUE;
			return TRUE;
		}
	}

	return FALSE;
}

/* start new game */
void 
new_game()
{
	srand(get_time());				/* for opening book randomization */
	setup_board(start_position);

	clear_main_hashtable();
	clear_pawn_hashtable();
}

/* print brief stats to console. used mostly for debugging */
void 
print_stats(int timediff)
{
	double search_time;
	double move_ordering;
	int nps;

	search_time = timediff / 100.0;
	move_ordering = counters.failed_high_total ? \
		(counters.failed_high_first / (double) counters.failed_high_total) * \
		100.0 : -1.0;
	nps = counters.searched_nodes / search_time;

	fprintf(stdout, "Time: %.2f, nodes: %llu, NPS: %d/sec, MO: %.2f%%\n" \
		"NULL: %llu, delta: %llu, LMR: %llu, razor: %llu, futility: %llu\n" \
		"pawn hash evaluations: %llu, transposition: %llu\n" \
	    "evaluations: %llu, lazy: %.2f%%, phase: %d\n",
		search_time, counters.searched_nodes, nps, move_ordering,
		counters.null_move_prunings, counters.delta_prunings,
		counters.reductions, counters.razorings, counters.futility_prunings,
		counters.pawn_hash_evaluations, counters.transposition, 
		counters.evaluations, ((double) counters.lazy_evaluations / counters.evaluations) * 100, GAME_PHASE);
}

/* if it's engine's turn to move we should search */
void *
process_turn(void *args)
{
	int move;
	int move_from_book;
	char buf[MAX_STRING];
	char ponder_candidate[32];

	return_ponder[0] = 0;

	move_from_book = TRUE;
	move = book_move();

	if (!move) {
		leave_book++;
		move_from_book = FALSE;
		set_engine_value(&e.thinking, TRUE);
		move = iterate(ponder_candidate);
		set_engine_value(&e.thinking, FALSE);
	} else
		leave_book = 0;
check_move:
	set_engine_value((int *)&e.ponderhit, FALSE);
	if (move == -1) {
		fprintf(stdout, "resign\n");
		if (get_engine_value(&e.side) == WHITE)
			fprintf(stdout, "0-1 {WHITE looses}\n");
		else
			fprintf(stdout, "1-0 {BLACK looses}\n");
		set_engine_value(&e.side, NOBODY);
		
		/* free resources after termination */
		pthread_detach(pthread_self());
		
		return (void *)0;
	}

	if (!move || evaluate_draw() || reps() >= 3) { 
		if (evaluate_draw()) {
			if (brd.fifty_rule >= 100)
				fprintf(stdout, "1/2-1/2 {Fifty move rule}\n");
			else
				fprintf(stdout, "1/2-1/2 {insufficient material}\n");
		} else if (reps() >= 3)
			fprintf(stdout, "1/2-1/2 {repetition}\n");
		else if (IS_CHECKED(WHITE))
			fprintf(stdout, "0-1 {BLACK mates}\n");
		else if (IS_CHECKED(BLACK))
			fprintf(stdout, "1-0 {WHITE mates}\n");
		else
			fprintf(stdout, "1/2-1/2 {Stalemate}\n");
		set_engine_value(&e.side, NOBODY);
	} else {
		if (get_engine_value(&e.san_notation) || \
				!get_engine_value(&e.xboard_mode)) {
			san_move(move, buf);
			make_move(move, FALSE);
			fprintf(stdout, "move %s\n", buf);

			/* create struct with all possible replies to simplify parsing
			 * later */
			ZERO_MEM(pm);

			int i;
			char parse_buf[MAX_STRING];
			struct moves_t moves_parse;

			gen_legal_moves(&moves_parse);

			/* generate pseudolegal moves and find target move */
			for (i = 0; i < moves_parse.count; i++) {
				pm.count++;
				pm.move[i] = moves_parse.move[i];
				strcpy(pm.sans[i], san_move(moves_parse.move[i], parse_buf));
			}

			if (ponder_candidate[0]) {
				make_move(parse_move_san(ponder_candidate), FALSE);
				if (book_move())
					ponder_candidate[0] = 0;
				takeback();
			}

			/* do we have a move to ponder? */
			if (ponder_candidate[0]) {
				make_move(parse_move_san(ponder_candidate), FALSE);

				/* don't ponder if a candidate is from opening book */
				/* ponder! */
				strncpy(return_ponder, ponder_candidate, 7);
				set_engine_value(&e.ponderhit, FALSE);
				set_engine_value(&e.thinking, TRUE);
				set_engine_value(&e.pondering, TRUE);
				move = iterate(ponder_candidate);
				set_engine_value(&e.thinking, FALSE);

				if (move != -1) {
					if (get_engine_value(&e.pondering)) {
						pthread_mutex_lock(&e.ponder_mutex);
						pthread_cond_wait(&cond_exit, &e.ponder_mutex);
						pthread_mutex_unlock(&e.ponder_mutex);
					}
					if (get_engine_value(&e.ponderhit))
						goto check_move;
				} else {
					if (get_engine_value(&e.ponderhit) ||
							get_engine_value(&e.stop_time))
						takeback();
					return_ponder[0] = 0;
					set_engine_value((int *)&e.pondering, 0);
				}
			} else
				return_ponder[0] = 0;

		} else {
			make_move(move, FALSE);
			fprintf(stdout, "move %s\n", coord_move(move, buf));
		}

		if (!get_engine_value(&e.xboard_mode) && !move_from_book)
			fprintf(stdout, "[%s]$ ", program_name);
	}
	/* free resources after termination */
	pthread_detach(pthread_self());

	return (void *)0;
}

void 
start_engine(void)
{
	pthread_mutex_init(&e.mutex, 0);
	pthread_mutex_init(&e.ponder_mutex, 0);

	set_engine_value(&e.side, NOBODY);
	set_engine_value(&e.force_mode, TRUE);
	set_engine_value(&e.post_mode, FALSE);
	set_engine_value(&e.xboard_mode, FALSE);
	set_engine_value(&e.san_notation, FALSE);
	set_engine_value(&e.ponder_allowed, TRUE);

	/* set small limits for quick testing */
	set_engine_value(&e.max_depth, 9);
	set_engine_value(&e.max_time, 10000);
	set_engine_value(&e.inc_time, 0);

	/* check if we should abort search every 1024 nodes */
	set_engine_value(&e.chk_time, 1023);

	/* use 1/30 of total time for thinking */
	set_engine_value(&e.time_div, 30);

	set_engine_value(&e.fixed_time, FALSE);
	set_engine_value((int *)&e.stop_time, 0);
	set_engine_value((int *)&e.max_stop_time, 0);

	/* if FALSE we are playing to mate */
	set_engine_value(&e.resign, TRUE); 
	set_engine_value(&e.resign_value, -600);

	/* if EPD tests are currently running */
	set_engine_value(&e.thinking, FALSE);
	set_engine_value(&e.pondering, FALSE);
	set_engine_value(&e.ponderhit, FALSE);

	/* search options */
	set_engine_value(&e.delta_pruning, TRUE);		
	set_engine_value(&e.futility_pruning, TRUE);
	set_engine_value(&e.iid, TRUE);
	set_engine_value(&e.null_pruning, TRUE);
	set_engine_value(&e.lmr, TRUE);
	set_engine_value(&e.razoring, TRUE);

	/* used to calculate 'game phase' */
	set_engine_value(&e.phase_factor, (piece_value[KNIGHT] * 2 + \
			piece_value[BISHOP] * 2 + piece_value[ROOK] * 2 + \
			piece_value[QUEEN]) / 128);

	e.tid =  0;

	/* initialize hashtable */
	e.pawn_hash_size = 2;
	e.main_hash_size = 16;
	e.pawn_hash_enabled = TRUE;
	e.main_hash_enabled = TRUE;

	book_open(book_file_name);
	new_game();
}

/* free resources */
void perform_cleanup(void)
{
	/* free memory */
	if (get_engine_value(&e.pawn_hash_enabled))
		free_pawn_hashtable();

	/* destroy engine mutex */
	pthread_mutex_destroy(&e.mutex);
	pthread_mutex_destroy(&e.ponder_mutex);

	/* close book file descriptor */
	book_close();
}
