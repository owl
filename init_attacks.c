/*
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#include "common.h"
#include "attacks.h"
#include "board.h"

/*
 *
 * Glaurung code. Magic bitboard calculation.
 *
 */

uint64_t rook_mult[64] = { 
	0xa8002c000108020ULL,  0x4440200140003000ULL, 0x8080200010011880ULL, 
	0x380180080141000ULL,  0x1a00060008211044ULL, 0x410001000a0c0008ULL, 
	0x9500060004008100ULL, 0x100024284a20700ULL,  0x802140008000ULL, 
	0x80c01002a00840ULL,   0x402004282011020ULL,  0x9862000820420050ULL, 
	0x1001448011100ULL,    0x6432800200800400ULL, 0x40100010002000cULL, 
	0x2800d0010c080ULL,    0x90c0008000803042ULL, 0x4010004000200041ULL, 
	0x3010010200040ULL,    0xa40828028001000ULL,  0x123010008000430ULL, 
	0x24008004020080ULL,   0x60040001104802ULL,   0x582200028400d1ULL, 
	0x4000802080044000ULL, 0x408208200420308ULL,  0x610038080102000ULL, 
	0x3601000900100020ULL, 0x80080040180ULL,	  0xc2020080040080ULL, 
	0x80084400100102ULL,   0x4022408200014401ULL, 0x40052040800082ULL, 
	0xb08200280804000ULL,  0x8a80a008801000ULL,   0x4000480080801000ULL, 
	0x911808800801401ULL,  0x822a003002001894ULL, 0x401068091400108aULL, 
	0x4a10a00004cULL,	   0x2000800640008024ULL, 0x1486408102020020ULL, 
	0x100a000d50041ULL,    0x810050020b0020ULL,   0x204000800808004ULL, 
	0x20048100a000cULL,    0x112000831020004ULL,  0x9000040810002ULL, 
	0x440490200208200ULL,  0x8910401000200040ULL, 0x6404200050008480ULL, 
	0x4b824a2010010100ULL, 0x4080801810c0080ULL,  0x400802a0080ULL, 
	0x8224080110026400ULL, 0x40002c4104088200ULL, 0x1002100104a0282ULL, 
	0x1208400811048021ULL, 0x3201014a40d02001ULL, 0x5100019200501ULL, 
	0x101000208001005ULL,  0x2008450080702ULL,	  0x1002080301d00cULL, 
	0x410201ce5c030092ULL
};

int rook_shift[64] = { 
	52, 53, 53, 53, 53, 53, 53, 52, 53, 54, 54, 54, 54, 54, 54, 53, 
	53, 54, 54, 54, 54, 54, 54, 53, 53, 54, 54, 54, 54, 54, 54, 53, 
	53, 54, 54, 54, 54, 54, 54, 53, 53, 54, 54, 54, 54, 54, 54, 53, 
	53, 54, 54, 54, 54, 54, 54, 53, 52, 53, 53, 53, 53, 53, 53, 52
};

int rook_index[64];
uint64_t rook_mask[64];
uint64_t rook_attacks[0x19000];

int rook_offsets[4][2] = {
	{0,1}, {0,-1}, {1,0}, {-1,0}
};

uint64_t bishop_mult[64] = {
	0x440049104032280ULL,  0x1021023c82008040ULL, 0x404040082000048ULL,
	0x48c4440084048090ULL, 0x2801104026490000ULL, 0x4100880442040800ULL,
	0x181011002e06040ULL,  0x9101004104200e00ULL, 0x1240848848310401ULL,
	0x2000142828050024ULL, 0x1004024d5000ULL,	  0x102044400800200ULL,
	0x8108108820112000ULL, 0xa880818210c00046ULL, 0x4008008801082000ULL,
	0x60882404049400ULL,   0x104402004240810ULL,  0xa002084250200ULL,
	0x100b0880801100ULL,   0x4080201220101ULL,	  0x44008080a00000ULL,
	0x202200842000ULL,	   0x5006004882d00808ULL, 0x200045080802ULL,
	0x86100020200601ULL,   0xa802080a20112c02ULL, 0x80411218080900ULL,
	0x200a0880080a0ULL,    0x9a01010000104000ULL, 0x28008003100080ULL,
	0x211021004480417ULL,  0x401004188220806ULL,  0x825051400c2006ULL,
	0x140c0210943000ULL,   0x242800300080ULL,	  0xc2208120080200ULL,
	0x2430008200002200ULL, 0x1010100112008040ULL, 0x8141050100020842ULL,
	0x822081014405ULL,	   0x800c049e40400804ULL, 0x4a0404028a000820ULL,
	0x22060201041200ULL,   0x360904200840801ULL,  0x881a08208800400ULL,
	0x60202c00400420ULL,   0x1204440086061400ULL, 0x8184042804040ULL,
	0x64040315300400ULL,   0xc01008801090a00ULL,  0x808010401140c00ULL,
	0x4004830c2020040ULL,  0x80005002020054ULL,   0x40000c14481a0490ULL,
	0x10500101042048ULL,   0x1010100200424000ULL, 0x640901901040ULL,
	0xa0201014840ULL,	   0x840082aa011002ULL,   0x10010840084240aULL,
	0x420400810420608ULL,  0x8d40230408102100ULL, 0x4a00200612222409ULL,
	0xa08520292120600ULL
};

int bishop_shift[64] = {
	58, 59, 59, 59, 59, 59, 59, 58, 59, 59, 59, 59, 59, 59, 59, 59,
	59, 59, 57, 57, 57, 57, 59, 59, 59, 59, 57, 55, 55, 57, 59, 59,
	59, 59, 57, 55, 55, 57, 59, 59, 59, 59, 57, 57, 57, 57, 59, 59,
	59, 59, 59, 59, 59, 59, 59, 59, 58, 59, 59, 59, 59, 59, 59, 58
};

int bishop_index[64];
uint64_t bishop_mask[64];
uint64_t bishop_attacks[0x1480];

int bishop_offsets[4][2] = {
	{1,1}, {-1,1}, {1,-1}, {-1,-1}
};

uint64_t 
index_to_bitboard(int index, uint64_t mask)
{
	int i, j, count;
	uint64_t bb;

	bb = 0ULL;
	count = popcount(mask);

	for (i = 0; i < count; i++) {
		j = bit_scan_clear(&mask);

		if (index & (1 << i))
			bb |= BIT(j);
	}

	return bb;
}

uint64_t 
sliding_attacks(int square, uint64_t block, int dirs, int deltas[][2], \
		int fmin, int fmax, int rmin, int rmax) 
{
	uint64_t result = 0ULL;
	int i, f, r;
	int dx, dy;
	int rank, file;

	result = 0ULL;
	rank = square / 8;
	file = square % 8;

	for (i = 0; i < dirs; i++) {
		dx = deltas[i][0]; 
		dy = deltas[i][1];

		for (f = file + dx, r = rank + dy;
			(!dx || (f >= fmin && f <= fmax)) && \
			(!dy || (r >= rmin && r <= rmax)); f += dx, r += dy) {
			
			result |= BIT(f + r * 8);

			if(block & BIT(f + r * 8))
				break;
		}
	}

	return result;
}

void 
initialize_bishop_attacks()
{
	int i, j, k;
	int index;
	uint64_t b;

	index = 0;

	for (i = 0; i < 64; i++) {
		bishop_index[i] = index;
		bishop_mask[i] = sliding_attacks(i, 0ULL, 4, bishop_offsets, \
				1, 6, 1, 6);
		j = BIT(64 - bishop_shift[i]);

		for (k = 0; k < j; k++) {
			b = index_to_bitboard(k, bishop_mask[i]);
			bishop_attacks[index + ((b * bishop_mult[i]) >> \
				bishop_shift[i])] = sliding_attacks(i, b, 4, \
				bishop_offsets, 0, 7, 0, 7);
		}
		index += j;
	}
}

void 
initialize_rook_attacks()
{
	int i, j, k;
	int index;
	uint64_t b;

	index = 0;

	for (i = 0; i < 64; i++) {
		rook_index[i] = index;
		rook_mask[i] = sliding_attacks(i, 0ULL, 4, rook_offsets, 1, 6, 1, 6);
		j = BIT(64 - rook_shift[i]);

		for (k = 0; k < j; k++) {
			b = index_to_bitboard(k, rook_mask[i]);
			rook_attacks[index + ((b * rook_mult[i]) >> rook_shift[i])] = \
				sliding_attacks(i, b, 4, rook_offsets, 0, 7, 0, 7);
		}
		index += j;
	}
}

void 
initialize_attacks()
{
	initialize_rook_attacks();
	initialize_bishop_attacks();
}
