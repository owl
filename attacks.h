/*
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

extern uint64_t rook_mult[64];
extern int rook_shift[64];
extern int rook_index[64];
extern uint64_t rook_mask[64];
extern uint64_t rook_attacks[0x19000];

extern uint64_t bishop_mult[64];
extern int bishop_shift[64];
extern int bishop_index[64];
extern uint64_t bishop_mask[64];
extern uint64_t bishop_attacks[0x1480];

/* bitboard of attacked squares by rook on square s */
#define ROOK_ATTACKS(s, blockers) (rook_attacks[rook_index[s] + \
		((((blockers) & rook_mask[s]) * rook_mult[s]) >> rook_shift[s])])

/* bitboard of attacked squares by bishop on square s */
#define BISHOP_ATTACKS(s, blockers) (bishop_attacks[bishop_index[s] + \
		((((blockers) & bishop_mask[s]) * bishop_mult[s]) >> bishop_shift[s])])

/* bitboard of attacked squares by queen on square s */
#define QUEEN_ATTACKS(s, blockers) (ROOK_ATTACKS((s), (blockers)) | \
		BISHOP_ATTACKS((s), (blockers)))

/* is side checked? */
#define IS_CHECKED(side) (IS_ATTACKED(brd.kings[side], 1 ^ (side)))

/* is square attacked by side */
#define IS_ATTACKED(square, side) ((KNIGHTS(side) & piece_moves[KNIGHT][square]) || \
		(BISHOP_ATTACKS(square, complete) & (BISHOPS(side) | QUEENS(side))) || \
		(ROOK_ATTACKS(square, complete) & (ROOKS(side) | QUEENS(side))) || \
		(KING(side) & piece_moves[KING][square]) || \
		(PAWNS(side) & pawn_moves[1 ^ (side)][square]))

/* returns bitboard of pieces attacking specified square */
#define ATTACK_TO(square, side, occ) ( \
	(KNIGHTS(side) & piece_moves[KNIGHT][square]) | \
	(KING(side) & piece_moves[KING][square]) | \
	(PAWNS(side) & pawn_moves[1 ^ (side)][square]) | \
	(((BISHOP_ATTACKS(square, occ)) & (BISHOPS(side) | QUEENS(side))) | \
	((ROOK_ATTACKS(square, occ)) & (ROOKS(side) | QUEENS(side)))))
