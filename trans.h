/*
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

enum HASHTABLE_FLAGS { INVALID, EXACT, LOWER, UPPER };

/* 
 * move:  mdf & 0x1FFFFF;
 * depth: (mdf >> 21) & 0xFF
 * flag:  mdf >> 29
 */
struct main_hash_entry_t {
	uint64_t key;
	int16_t score;
	uint16_t age;
	uint32_t mdf;
};

struct pawn_hash_entry_t {
	uint64_t key;
	uint64_t passers;
	int16_t score_op;
	int16_t score_eg;
};

/* age of transposition table (for replacement strategy) */
extern uint16_t hashtable_age;

extern uint64_t pawn_hash_mask;
extern uint64_t main_hash_mask;

extern struct pawn_hash_entry_t *pawn_hash[2];
extern struct main_hash_entry_t *main_hash;

int initialize_main_hashtable(void);
int initialize_pawn_hashtable(void);

int hashtable_get(int side, int depth, int ply, int *move, int *score);
void hashtable_put(int side, int depth, int ply, int flag, int move, \
		int score);

void clear_main_hashtable(void);
void clear_pawn_hashtable(void);
void free_main_hashtable(void);
void free_pawn_hashtable(void);
