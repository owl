/*
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#include "common.h"
#include "board.h"
#include "engine.h"

const char *char_board[64] = {
	"a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1",
	"a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2",
	"a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3",
	"a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4",
	"a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5",
	"a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6",
	"a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7",
	"a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8" };

const char char_pieces[7] = {' ', 'P', 'N', 'B', 'R', 'Q', 'K'};

/* helper data */
int lzArray[65536];
int bitcount[65536];

uint64_t isolated_mask[8];		/* masks adjacent files */
uint64_t passer_mask[2][64];	/* masks passed pawn way */
uint64_t pawn_moves[2][64];		/* pawn capture moves */
uint64_t piece_moves[7][64];	/* all piece moves */
uint64_t forward_ray[2][64];	/* masks squares infront of a pawn */
uint64_t king_shield[2][64];	/* example: for king on G1 - F2,g2,h2 */

int distance[64][64];			/* distance from 2 squares */

/* bitboard mask for files */
const uint64_t file_bit[8] = {
	0x0101010101010101ULL, 0x0202020202020202ULL,
	0x0404040404040404ULL, 0x0808080808080808ULL,
	0x1010101010101010ULL, 0x2020202020202020ULL,
	0x4040404040404040ULL, 0x8080808080808080ULL
};

uint64_t complete;					/* complete bitboard for both sides */
int square64[64];					/* regular board */
struct board_t brd;					/* board parameters */
struct game_history_t game_history;	/* takeback buffer */

uint64_t piece_boards[2][7];	/* bitboard for each type of piece */
uint64_t side_boards[2];		/* pieces for each side */

int castled[2];					/* WHITE or BLACK castled */

int material_complete[2];		/* sum of the material */
int material_pawns[2];			/* ... for pawns */
int material_pieces[2];			/* for pieces except pawns */

/* initial values according to Larry Kaufman */
int piece_value[7] = { 0, 100, 325, 325, 500, 1000, 10000 };

int piece_count[2][7];			/* count pieces of each type */

/* add piece to the board, update bitboard, zobrist and count */
void 
add_piece(int square, int piece, int side)
{
	assert(square >= 0 && square <= 63);
	assert(piece >= PAWN && piece <= KING);
	assert(side == WHITE || side == BLACK);
	assert(material_complete[side] == material_pieces[side] + \
			material_pawns[side]);

	square64[square] = piece;

	SET(piece_boards[side][piece], square);			/* update bitboard */
	SET(side_boards[side], square);					/* update bitboard */
	SET(complete, square);							/* update bitboard */
	brd.hash_key ^= hash_code[side][piece][square];	/* update zobrist key */

	if (piece == KING)
		brd.kings[side] = square;
	else {
		if (piece == PAWN) {
			brd.hash_pawn_key ^= hash_code[side][PAWN][square];
			material_pawns[side] += piece_value[PAWN];
		} else
			material_pieces[side] += piece_value[piece];
		material_complete[side] += piece_value[piece];
	}
	piece_count[side][piece]++;
}

int
board_is_legal(struct board_t *b)
{
	int i;
	int wk = 0;
	int bk = 0;
	int color;
	int n_pieces = 0;

	for (i = 0; i < 64; i++) {
		if (!square64[i]) {
			/* if mailbox square isn't empty but bitboard's bit isn't set */
			if (complete & BIT(i))
				return FALSE;
		} else {
			color = -1;    
			switch (square64[i]) {
				case PAWN:
					if (((PAWNS(WHITE) | PAWNS(BLACK)) & BIT(i)) == 0)
						return FALSE;
					if (_FILE(square64[i]) == 0 || _FILE(square64[i]) == 7)
						return FALSE;
					break;
				case KNIGHT:
					if (((KNIGHTS(WHITE) | KNIGHTS(BLACK)) & BIT(i)) == 0)
						return FALSE;
					break;
				case BISHOP:
					if (((BISHOPS(WHITE) | BISHOPS(BLACK)) & BIT(i)) == 0)
						return FALSE;
					break;
				case ROOK:
					if (((ROOKS(WHITE) | ROOKS(BLACK)) & BIT(i)) == 0)
						return FALSE;
					break;
				case QUEEN:
					if (((QUEENS(WHITE) | QUEENS(BLACK)) & BIT(i)) == 0)
						return FALSE;
					break;
				case KING:
					if (b->kings[WHITE] == i)
						wk++;
					else if (b->kings[BLACK] == i)
						bk++;
					break;
				default:
					return FALSE;
			}
			n_pieces++;
		}
	}
	/* bad king count or more than 32 pieces total */
	if (wk != 1 || bk != 1 || n_pieces > 32)
		return FALSE;

	return TRUE;
}

/* return board position in Forsyth-Edwards notation */
char *
current_fen_position(char *buf)
{
	assert(board_is_legal(&brd));

	char tmp[256];
	int i, j, p;
	int empty;

	empty = 0;
	buf[0] = '\0';

	for (i = 8; i > 0; i--) {
		for (j = 0; j < 8; j++) {
			p = square64[(i - 1) * 8 + j];
			if (!p)
				empty++;
			else {
				if (empty) {
					/* square is not empty, but last square is. add empty
					   square count to result */
					snprintf(tmp, 2, "%d", empty);
					strcat(buf, tmp);
				}
				if (side_boards[WHITE] & BIT((i - 1) * 8 + j))
					sprintf(tmp, "%c", char_pieces[p]);
				else
					sprintf(tmp, "%c", (char)(char_pieces[p] + 'a' - 'A'));
				strcat(buf, tmp);
				empty = 0;
			}
		}
		if (empty) {
			snprintf(tmp, 2, "%d", empty);
			strcat(buf, tmp);
			empty = 0;
		}
		if (i != 1)
			strcat(buf, "/");
	}

	strcat(buf, !brd.wtm ? " w " : " b ");

	if (!brd.castle_mask)
		strcat(buf, "-");
	else {
		if (brd.castle_mask & WHITE_CASTLE_KINGSIDE)
			strcat(buf, "K");
		if (brd.castle_mask & WHITE_CASTLE_QUEENSIDE)
			strcat(buf, "Q");
		if (brd.castle_mask & BLACK_CASTLE_KINGSIDE)
			strcat(buf, "k");
		if (brd.castle_mask & BLACK_CASTLE_QUEENSIDE)
			strcat(buf, "q");
	}

	strcat(buf, " ");

	if (brd.ep == -1)
		strcat(buf, "-");
	else
		strcat(buf, char_board[brd.ep]);

	return buf;
}

/* remove piece from the board, update bitboard, zobrist and count */
void 
remove_piece(int square, int piece, int side)
{
	assert(square >= 0 && square <= 63);
	assert(piece >= PAWN && piece <= KING);
	assert(side == WHITE || side == BLACK);
	assert(material_complete[side] == material_pieces[side] + \
			material_pawns[side]);

	square64[square] = 0;

	CLEAR(piece_boards[side][piece], square);		/* update bitboard */
	CLEAR(side_boards[side], square);				/* update bitboard */
	CLEAR(complete, square);						/* update bitboard */
	brd.hash_key ^= hash_code[side][piece][square];	/* update zobrist */

	if (piece != KING) {
		if (piece == PAWN) {
			brd.hash_pawn_key ^= hash_code[side][PAWN][square];
			material_pawns[side] -= piece_value[PAWN];
		} else
			material_pieces[side] -= piece_value[piece];
		material_complete[side] -= piece_value[piece];
	}
	piece_count[side][piece]--;
}

/* how many times this position was occured */
int 
reps()
{
	int i, rep;

	/* search for the same hash_key */
	for (i = 0, rep = 0; i < game_history.count - 1; i++)
		if (game_history.board[i].hash_key == brd.hash_key)
			rep++;

	return rep;
}

/* setup board using FEN notation */
int 
setup_board(char *fen)
{
	int i, j, k, m, p;
	int color;
	char ranks[8][64];
	char side[8];
	char castle[8];
	char ep[8];

	if (sscanf(fen, "%8[1-8pnbrqkPNBRQK]/%8[1-8pnbrqkPNBRQK]/" \
			"%8[1-8pnbrqkPNBRQK]/%8[1-8pnbrqkPNBRQK]/%8[1-8pnbrqkPNBRQK]/" \
			"%8[1-8pnbrqkPNBRQK]/%8[1-8pnbrqkPNBRQK]/%8[1-8pnbrqkPNBRQK] " \
			"%1[bw] %[KQkq-] %[1-8a-h-]", ranks[7], ranks[6], ranks[5], \
			ranks[4], ranks[3], ranks[2], ranks[1], ranks[0], side, \
			castle, ep) < 11)
		return FALSE;

	ZERO_MEM(brd);
	ZERO_MEM(game_history);
	ZERO_MEM(piece_boards);
	ZERO_MEM(side_boards);
	ZERO_MEM(material_pawns);
	ZERO_MEM(material_pieces);
	ZERO_MEM(material_complete);
	ZERO_MEM(piece_count);

	castled[WHITE] = castled[BLACK] = FALSE;

	for (i = 0, j = 0; i < 8; i++, j = 0) 
		for (p = 0; p < strlen(ranks[i]); p++) {
			if (ranks[i][p] < 'A') {			/* empty squares */
				for (m = j, k = m; k < m + ranks[i][p] - '0'; k++, j++)
					square64[i * 8 + j] = 0;
				continue;
			}
			/* lowercase letter - BLACK, uppercase - WHITE */
			color = ranks[i][p] > 'a'? BLACK : WHITE;
			switch (ranks[i][p] - ('a' - 'A') * color) {
				case 'B':
					add_piece(i * 8 + j, BISHOP, color);
					break;
				case 'K':
					add_piece(i * 8 + j, KING, color);
					break;
				case 'N':
					add_piece(i * 8 + j, KNIGHT, color);
					break;
				case 'P':
					add_piece(i * 8 + j, PAWN, color);
					break;
				case 'Q':
					add_piece(i * 8 + j, QUEEN, color);
					break;
				case 'R':
					add_piece(i * 8 + j, ROOK, color);
					break;
			}
			j++;
		}

	brd.wtm = (!strcmp(side, "w")) ? WHITE : BLACK;
	
	brd.ep = -1;
	if (strncmp(ep, "-", 1)) {
		i = (ep[0] - 'a') + (ep[1] - '1') * 8;
		j = i + (brd.wtm == WHITE? 8 : -8);
		/* polyglot compatibility hack */
		if ((BIT(j - 1) & PAWNS(brd.wtm)) || (BIT(j + 1) & PAWNS(brd.wtm)))
			brd.ep = i;
	}
	assert((brd.ep >= 16 && brd.ep <= 47) || brd.ep == -1);

	brd.castle_mask = 0;

	if (strchr(castle, 'K'))
		brd.castle_mask |= WHITE_CASTLE_KINGSIDE;
	if (strchr(castle, 'Q'))
		brd.castle_mask |= WHITE_CASTLE_QUEENSIDE;
	if (strchr(castle, 'k'))
		brd.castle_mask |= BLACK_CASTLE_KINGSIDE;
	if (strchr(castle, 'q'))
		brd.castle_mask |= BLACK_CASTLE_QUEENSIDE;

	brd.fifty_rule = 0;

	update_boards();
	calc_zobrist(&brd.hash_key, &brd.hash_pawn_key);

	assert(board_is_legal(&brd));
	set_engine_value(&e.hopeless_moves, 0);

	return TRUE;
}

/* update bitboards, normaly after making moves */
void 
update_boards(void)
{
	side_boards[WHITE] = PAWNS(WHITE) | KNIGHTS(WHITE) | BISHOPS(WHITE) | \
					  ROOKS(WHITE) | QUEENS(WHITE) | KING(WHITE);
	side_boards[BLACK] = PAWNS(BLACK) | KNIGHTS(BLACK) | BISHOPS(BLACK) | \
					  ROOKS(BLACK) | QUEENS(BLACK) | KING(BLACK);

	complete = side_boards[WHITE] | side_boards[BLACK];
}

/* simple board representation for debugging */
void 
print_bb(uint64_t b)
{
	int rank, file;

	for (rank = 7; rank >= 0; rank--) {
		for (file = 0; file < 8; file++)
			if (b & (1ULL << (rank * 8 + file)))
				printf("* ");
			else
				printf("X ");
		printf("\n");
	}	
	printf("\n");
}

/* simple bitboard representation for debugging */
void 
print_board()
{
	int i, j, p;
	char pieces[] = {' ', 'P', 'N', 'B', 'R', 'Q', 'K'};

	for (i = 7; i >= 0; i--) {
		printf("%d  ", i + 1);
		for (j = 0; j < 8; j++) {
			p = square64[i * 8 + j];
			if (p) {
				if (side_boards[BLACK] & BIT(i * 8 + j))
					printf("%c ", (char)(pieces[p] + ('a' - 'A')));
				else
					printf("%c ", pieces[p]);
			} else
				printf(". ");
		}
		printf("\n");
	}
	printf("\n   a b c d e f g h\n");
}
