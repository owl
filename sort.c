/*
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#include "common.h"
#include "move.h"
#include "engine.h"

void 
sort_moves(struct moves_t *moves, int current, int ply)
{
	int i;
	int high;

	assert(current >= 0 && current <= MAX_MOVES);

	high = current;
	for (i = current; i < moves->count; i++) {
		assert(move_is_legal(moves->move[i]));

		if (moves->move[i] == hash_moves[ply]) {
			high = i;
			break;
		}

		if (killers[ply].mate_killer == moves->move[i])
			moves->score[i] += 15000;
		if (killers[ply].killer1 == moves->move[i] || \
				killers[ply].killer2 == moves->move[i])
			moves->score[i] += 5000;

		if (moves->score[i] > moves->score[high])
			high = i;
	}

	int tmp_move = moves->move[high];
	int tmp_score = moves->score[high];
	int tmp_see = moves->see[high];

	moves->move[high] = moves->move[current];
	moves->score[high] = moves->score[current];
	moves->see[high] = moves->see[current];

	moves->move[current] = tmp_move;
	moves->score[current] = tmp_score;
	moves->see[current] = tmp_see;
}
