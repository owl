/*
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#include "common.h"
#include "board.h"

static const uint64_t de_bruijn_c = 0x07EDD5E59A4E28C2ULL;

static const int de_bruijn_index[64] = {
	63,  0, 58,  1, 59, 47, 53,  2,
	60, 39, 48, 27, 54, 33, 42,  3,
	61, 51, 37, 40, 49, 18, 28, 20,
	55, 30, 34, 11, 43, 14, 22,  4,
	62, 57, 46, 52, 38, 26, 32, 41,
	50, 36, 17, 19, 29, 10, 13, 21,
	56, 45, 25, 31, 35, 16,  9, 12,
	44, 24, 15,  8, 23,  7,  6,  5
};

int 
bit_scan(uint64_t b)
{
	return de_bruijn_index[((b & -b) * de_bruijn_c) >> 58];
}

int 
bit_scan_clear(uint64_t *b)
{
	uint64_t bb;

	bb = *b;
	*b &= *b - 1;

	return de_bruijn_index[((bb & -bb) * de_bruijn_c) >> 58];
}

int 
bit_scan_rev(uint64_t b)
{
	if (b >> 48) return (lzArray[b >> 48]) ^ 63;
	if (b >> 32) return (lzArray[b >> 32] + 16) ^ 63;
	if (b >> 16) return (lzArray[b >> 16] + 32) ^ 63;

	return (lzArray[b] + 48) ^ 63;
}
