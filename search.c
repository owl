/*
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#include "common.h"
#include "attacks.h"
#include "board.h"
#include "engine.h"
#include "evaluate.h"
#include "move.h"
#include "search.h"
#include "trans.h"

/* save move to killers if needed and update history */
void 
history_store(int move, int depth, int ply, int score)
{
	history[brd.wtm][PIECE(move)][move & 0xFFF] += depth * depth;

	if (SCORE_IS_MATE(score)) {
		/* save mate killer separately */
		if (!killers[ply].mate_killer)
			killers[ply].mate_killer = move;
	} else
		if (killers[ply].killer1 != move) {
			killers[ply].killer2 = killers[ply].killer1;
			killers[ply].killer1 = move;
		}
}

/* search for PV */
int 
search_pv(int alpha, int beta, int depth, int ply, int check)
{
	int i, j;
	int old_alpha;
	int extend;
	int score;
	int is_check;
	int moves_count;
	int searching_pv;
	struct moves_t moves;

	assert(alpha >= -MATE_VALUE && alpha < +MATE_VALUE);
	assert(beta > alpha && beta <= +MATE_VALUE);
	assert(depth >= 0 && depth <= MAX_PLY);
	assert(board_is_legal(&brd));

	searching_pv = TRUE;
	moves_count = 0;

	old_alpha = alpha;
	pv_length[ply] = ply;

	if (check_time(ply)) 
		return 65535;

	if (reps() || evaluate_draw())
		return 0;

	/* if we are too deep */
	if (ply >= MAX_PLY - 1)
		return evaluate(alpha, beta);

	if (depth <= 0 && !check)
		return quiesce(alpha, beta, 0, ply, 0);

	gen_moves(&moves);

	hash_moves[ply] = 0;

	/* always try to follow previous PV */
	if (follow_pv) {
		follow_pv = FALSE;
		for (i = 0; i < moves.count; i++)
			if (moves.move[i] == pv[0][ply]) {
				follow_pv = TRUE;
				hash_moves[ply] = pv[0][ply];
				break;
			}
	}

	if (!hash_moves[ply] && e.main_hash_enabled)
		if (hashtable_get(brd.wtm, depth, ply, &hash_moves[ply], &score)) {
#ifdef STATISTIC_COUNTERS
			counters.transposition++;
#endif
		}

	/* internal iterative deeping if we haven't hash move */
	if (!hash_moves[ply] && get_engine_value(&e.iid) && depth > 3) {
		i = MIN(depth - 2, depth / 2); 

		score = search_pv(alpha, beta, i, ply, check);
		if (abort_search) 
			return 65535;

		if (score <= alpha) {
			score = search_pv(-MATE_VALUE, beta, i, ply, check);
			if (abort_search) 
				return 65535;
		}
		hash_moves[ply] = pv[ply][ply];
	}

	for (i = 0; i < moves.count; i++) {
		/* get move with largest score */
		sort_moves(&moves, i, ply);
		if (!make_move(moves.move[i], FALSE))
			continue;

		moves_count++;

		is_check = IS_CHECKED(brd.wtm);

		/* search first move with full window */
		if (searching_pv) {
			score = -search_pv(-beta, -alpha, depth + check - 1, ply + 1, is_check);
			if (abort_search) 
				return 65535;
		} else {
			/* examine if we can reduce or extend move */
			extend = moves_count > NOLMR_MOVES? \
					 get_extensions(moves.move[i], check, is_check, depth, ply) : \
					 check;
			if (extend < 0) {
				extend = MIN(-MIN(depth - 2, (moves_count > 2)? 2 : 1), 0);
			}

			score = -zwsearch(-alpha, depth + extend - 1, ply + 1, is_check, 1);
			if (abort_search) 
				return 65535;

			if (score > alpha && extend < 0) {
				score = -zwsearch(-alpha, depth - 1, ply + 1, is_check, 1);
				if (abort_search) 
					return 65535;
			}

			if (score > alpha && score < beta) {
				int reset_fhr = FALSE;
				if (!fail_high_root && ply == 1) {
					fail_high_root = TRUE;
					reset_fhr = TRUE;
				}
				score = -search_pv(-beta, -alpha, depth + check - 1, ply + 1, is_check);
				if (reset_fhr)
					fail_high_root = FALSE;
				if (abort_search) 
					return 65535;
			}
		}

		takeback();

		searching_pv = FALSE;

		if (score > alpha) {
			if (score >= beta) {
#ifdef STATISTIC_COUNTERS
				counters.failed_high_total++;
				if (moves_count == 1)
					counters.failed_high_first++;
#endif
				if (!MOVE_IS_TACTICAL(moves.move[i]))
					history_store(moves.move[i], depth, ply, beta);

				return beta;
			}
			alpha = score;
			pv[ply][ply] = moves.move[i];

			for (j = ply + 1; j < pv_length[ply + 1]; j++)
				pv[ply][j] = pv[ply + 1][j];
			pv_length[ply] = pv_length[ply + 1];
		}
	}

	if (!moves_count)
		alpha = check? -MATE_VALUE + ply : 0;

	return alpha;
}
