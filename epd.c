/*
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#include "common.h"
#include "board.h"
#include "engine.h"
#include "move.h"
#include "search.h"
#include "trans.h"

/* TODO: better EPD knowlege */
enum EPD_TASKS { EPD_BEST_MOVE, EPD_AVOID_MOVE };

/* load EPD positions from file and solve them */
void *run_epd_test(void *args)
{
	int move;
	FILE *epd_file;
	struct epd_info_t *ei;
	char dummy[MAX_STRING];
	char target_move1[MAX_STRING];
	char target_move2[MAX_STRING];
	char epd_line[MAX_STRING];
	char epd_type[MAX_STRING];
	int epd_count, epd_processed, epd_solved, epd_task;

	/* args - struct epd_info_t */
	ei = (struct epd_info_t *)args;
	epd_file = fopen(ei->filename, "r");

	if (!epd_file) {
		fprintf(stderr, "EPD error: %s\n", strerror(errno));

		/* free memory */
		free(ei->filename);
		free(ei);
		return (void *)0;
	}

	epd_count = 0; 
	epd_processed = 0;
	epd_solved = 0;

	set_engine_value(&e.thinking, TRUE);
	set_engine_value(&e.fixed_time, TRUE);

	while (fgets(epd_line, MAX_STRING, epd_file)) {
		fprintf(stdout, "EPD: %s", epd_line);

		/* seek for first test */
		if (++epd_count < ei->first_test)
			continue;

		if (!setup_board(epd_line)) {
			/* handle incorrect lines */
			fprintf(stderr, "Invalid EPD!\n");
			continue;
		}

		int target_moves = sscanf(epd_line, "%s %s %s %s %s %[1-8a-hNBRQKx+#]"\
				" %[1-8a-hNBRQKx+#]", dummy, dummy, dummy, dummy, epd_type, \
				target_move1, target_move2) - 5;
		
		/* invalid epd position */
		if (target_moves < 1)
			continue;

		if (!strncmp(epd_type, "bm", 2))
			epd_task = EPD_BEST_MOVE;
		else if ((target_moves > 1) && !strncmp(epd_type, "am", 2))
			epd_task = EPD_AVOID_MOVE;
		else
			continue;

		epd_processed++;

		/* set maximum search depth and convert time from seconds */
		set_engine_value(&e.max_depth, 64);
		set_engine_value(&e.max_time, ei->think_time * 100);

		/* clear transposition table and start search */
		clear_main_hashtable();
		clear_pawn_hashtable();

		move = iterate(0);

		if (move) {
			san_move(move, dummy);

			/* did we found the move? */
			if (!strncmp(dummy, target_move1, 6)) {
				if (epd_task == EPD_BEST_MOVE)
					epd_solved++;
			} else if ((target_moves > 1) && !strncmp(dummy, target_move2, 6)) {
				if (epd_task == EPD_BEST_MOVE)
					epd_solved++;
			}
		}

		fprintf(stdout, "\n* Tested EPD's = %d, solved = %d, percent = %.2f%%, " \
				"total = %d.\n\n", epd_processed, epd_solved, \
				((float)epd_solved / epd_processed) * 100, epd_count);

		if (epd_count >= ei->max_tests)
			break;
	}

	set_engine_value(&e.fixed_time, FALSE);
	set_engine_value(&e.thinking, FALSE);

	/* cleanup */
	fclose(epd_file);
	free(ei->filename);
	free(ei);

	return (void *)0;
}
