/*
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#include "common.h"
#include "attacks.h"
#include "board.h"
#include "move.h"

/* move in SAN notation */
char *
san_move(int move, char *buf)
{
	int i;
	int from, to;
	char tmp[MAX_STRING];
	int piece, cap_piece, promote_piece, type;
	int move_legal, move_check, move_mate;
	struct moves_t moves;

	assert(move_is_legal(move));

	buf[0] = '\0';
	tmp[0] = '\0';

	from = GET_FROM(move);
	to = GET_TO(move);
	piece = PIECE(move);
	cap_piece = CAP_PIECE(move);
	promote_piece = GET_PROMOTE(move);
	type = GET_TYPE(move);

	move_legal = move_check = move_mate = FALSE;

	gen_legal_moves(&moves);

	for (i = 0; i < moves.count; i++) {
		if (make_move(moves.move[i], FALSE)) {
			if (move == moves.move[i]) {
				move_legal = TRUE;

				if (IS_CHECKED(brd.wtm)) {
					move_check = TRUE;

					if (!gen_legal_moves(&moves))
						move_mate = TRUE;
				}
				takeback();
				break;
			}
			takeback();
		}
	}

	if (!move_legal)
		return NULL;

	if (type & TYPE_CASTLE) {
		if (to == G1 || to == G8)
			sprintf(buf, "%s", "O-O");
		else
			sprintf(buf, "%s", "O-O-O");
	} else {
		if (piece == PAWN) {
			if (cap_piece || (type & TYPE_ENPASSANT))
				sprintf(buf, "%cx%s", (char) (_FILE(from) + 'a'), char_board[to]);
			else
				sprintf(buf, "%s", char_board[to]);
			if (promote_piece) {
				sprintf(tmp, "=%c", char_pieces[promote_piece]);
				strcat(buf, tmp);
			}
		} else {
			sprintf(buf, "%c", char_pieces[piece]);
			
			gen_legal_moves(&moves);

			for (i = 0; i < moves.count; i++) {
				if (move != moves.move[i]) {
					if (piece == PIECE(moves.move[i]) && to == GET_TO(moves.move[i])) {
						/* move is ambiguous */
						if (_FILE(from) == _FILE(GET_FROM(moves.move[i])))
							sprintf(tmp, "%c", (char) (_RANK(from) + '1'));
						else
							sprintf(tmp, "%c", (char) (_FILE(from) + 'a'));
						strcat(buf, tmp);
						break;
					}
				}
			}

			if (cap_piece)
				sprintf(tmp, "x%s", char_board[to]);
			else
				sprintf(tmp, "%s", char_board[to]);
			strcat(buf, tmp);
		}
	}

	if (move_mate)
		strcat(buf, "#");
	else if (move_check)
		strcat(buf, "+");

	return buf;
}
