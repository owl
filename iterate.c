/*
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#include "common.h"
#include "attacks.h"
#include "board.h"
#include "engine.h"
#include "evaluate.h"
#include "move.h"
#include "search.h"
#include "trans.h"

#define MAX_PV_STRING_SIZE	2048

int follow_pv;		/* if we are following PV searched by previous 
					   iteration */
int abort_search;	/* TRUE if search should be stopped by some reason */
int fail_high_root;	/* fail high at first ply */
int leave_book;
int safe_search;

/* iterative deeping */
int 
iterate(char *ponder_candidate)
{
	int engine_xboard;
	int best_move;
	int best_ponder;
	int best_score;
	int best_depth;
	int i_depth;
	int score;
	int start_time;
	struct moves_t moves;
	char pv_string[MAX_PV_STRING_SIZE];

	start_time = get_time();

	int max_time = get_engine_value(&e.max_time);
	int inc_time = (get_engine_value(&e.inc_time) * 90) / 100;
	int time_bonus = leave_book == 1 && !get_engine_value(&e.fixed_time)? max_time : 0;
	
	set_engine_value((int *)&e.stop_time, start_time + \
			max_time + time_bonus + inc_time);			/* when stop search */
	set_engine_value((int *)&e.max_stop_time, start_time + \
			e.max_time * 4 + time_bonus + inc_time);	/* when stop longest search */

	if (get_engine_value(&e.pondering))
		set_engine_value((int *)&e.max_depth, 64);

	/* initialize some data */
	ZERO_MEM(pv);
	ZERO_MEM(pv_length);
	ZERO_MEM(history);
	ZERO_MEM(killers);
	ZERO_MEM(hash_moves);
	ZERO_MEM(counters);

	if (ponder_candidate != NULL)
		ponder_candidate[0] = 0;

	best_move = 0;
	best_score = -MATE_VALUE;
	best_ponder = 0;
	best_depth = 0;
	abort_search = FALSE;
	fail_high_root = FALSE;

#ifdef CONSOLE_DEBUG
	char fen_position[256];
	fprintf(stdout, "setboard %s\n", current_fen_position(fen_position));
#endif
	hashtable_age++;

	if (evaluate_draw() && !get_engine_value(&e.pondering))
		return 0;
	else if (reps() >= 3 && !get_engine_value(&e.pondering))
		return 0;

	engine_xboard = get_engine_value(&e.xboard_mode);
	/* if we have the only one move, don't think about it */
	if (gen_legal_moves(&moves) == 1 && !get_engine_value(&e.pondering))
		return moves.move[0];

	if (!engine_xboard)
		printf("ply		  nodes score  pv\n");

	safe_search = FALSE;

	for (i_depth = 1; i_depth <= get_engine_value(&e.max_depth); i_depth++) {
		follow_pv = TRUE;
		score = search_root(-MATE_VALUE, +MATE_VALUE, i_depth, \
				IS_CHECKED(brd.wtm));

		if (abort_search)
			break;

		/* save best move from last successful iteration */
		best_move = pv[0][0];
		best_ponder = pv_length[0] > 1? pv[0][1] : 0;
		best_score = score;
		best_depth = i_depth;

		/* get string PV representation (for 'Show Thinking' option or console
		   output) */
		if (get_engine_value(&e.san_notation) || !engine_xboard)
			get_pv_string_san(pv_string);
		else
			get_pv_string_coord(pv_string);

		assert(score >= -MATE_VALUE && score <= MATE_VALUE);

		if (!engine_xboard && pv[0][0])
			fprintf(stdout, "%3d  %10llu %5s%.2f %5u   %s", i_depth, \
				counters.searched_nodes, score >= 0? "+" : "-", \
				abs(score) / (float)100, get_time() - start_time, pv_string);
		else if (engine_xboard && get_engine_value(&e.post_mode) && \
				((i_depth > 5) || SCORE_IS_MATE(score))) {
			fprintf(stdout, "%3d %7d %5u %10llu   %s",
					i_depth, score, get_time() - start_time, 
					counters.searched_nodes, pv_string);
		}

		if (SCORE_IS_MATE(score) && i_depth > 1)
			/* don't search further if we have confirmed mate */
			break;
		/* if it's not fixed time search and we used more than 50% of the
		   required time */
		if (!get_engine_value(&e.fixed_time) && ((get_time() - start_time) / \
				(double)(get_engine_value((int *)&e.stop_time) - start_time) > 0.5) &&
				!get_engine_value(&e.pondering))
			break;
		/* if it's not fixed time search and we used more than 30% of the
		   required time */
		if (!get_engine_value(&e.fixed_time) && ((get_time() - start_time) / \
					(double)(get_engine_value((int *)&e.stop_time) - start_time) > 0.33))
			safe_search = TRUE;
			
	}
	if (get_engine_value(&e.play_computer) && get_engine_value(&e.post_mode))
		fprintf(stdout, "tellics kibitz %.2f [%d], %dkNPS, PV = %s", (double)
			best_score/100, best_depth, (int)((counters.searched_nodes/ 1000) / ((get_time() -
			start_time) / (double)100)), pv_string);

	/* should we resign? */
	if (get_engine_value(&e.resign) && (best_score <= \
				get_engine_value(&e.resign_value))) {
		if (++e.hopeless_moves >= 3 && !get_engine_value(&e.pondering) &&
				!get_engine_value(&e.ponderhit))
			return -1;
	} else
		e.hopeless_moves = 0;

#ifdef CONSOLE_DEBUG
	print_stats(get_time() - start_time);
#endif
	if (get_engine_value(&e.pondering) || 
            !get_engine_value(&e.stop_time) ||
            !get_engine_value(&e.ponder_allowed))
		best_ponder = 0;

	if (ponder_candidate && best_ponder &&
			(!engine_xboard || 
			 (engine_xboard && get_engine_value(&e.post_mode))) &&
			!SCORE_IS_MATE(best_score)) {
		/* print a move that we could ponder */
		make_move(best_move, 0);
		printf("Ponder candidate: %s\n", san_move(best_ponder,
					ponder_candidate));
		takeback();
	}
	if ((SCORE_IS_MATE(best_score) || (i_depth == get_engine_value(&e.max_depth)))&&
			get_engine_value(&e.pondering) && !get_engine_value(&e.ponderhit)) {
		ponder_candidate[0] = 0;
		return -1;
	}

	return best_move;
}
