/*
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
void initialize_bitcount(void);
void initialize_lzarray(void);
void initialize_bitboards(void);
void initialize_ray(void);
void initialize_moves(void);
void initialize_pawn_moves(void);
void initialize_piece_moves(void);
void initialize_forward_ray(void);
void initialize_passers(void);
void initialize_isolated(void);
void initialize_distance(void);
void initialize_king_shield(void);
void initialize_king_moat(void);
void initialize_zobrist(void);
